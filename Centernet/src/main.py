from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
import _init_paths

import os 
os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
os.environ['CUDA_VISIBLE_DEVICES'] = "2,3" #"0,1,2"

import torch
# FIXME !!!
# 尝试不用cudnn
torch.backends.cudnn.enabled = False

print(torch.__version__)

import torch.utils.data
from opts import opts
from models.model import create_model, load_model, save_model
from models.data_parallel import DataParallel
from logger import Logger
from datasets.dataset_factory import get_dataset
from trains.train_factory import train_factory
from time import time
# FIXME !!!
# 尝试不用cudnn
torch.backends.cudnn.enabled = False

print(torch.__version__)
print(torch.version.cuda)


def main(opt):
    # 这里设置了随机数种子
    torch.manual_seed(opt.seed)
    # FIXME ？？/
    torch.backends.cudnn.benchmark = not opt.not_cuda_benchmark and not opt.test

    # 这里调用了get_dataset方法，返回的类继承了 采样类 和 数据集类 两个父类
    Dataset = get_dataset(opt.dataset, opt.task)

    # train_dataset = Dataset(opt, 'train')
    # val_dataset = Dataset(opt, 'val')

    # 根据数据集的属性，分辨率、均值、方差、类别数量 四个参数。
    opt = opts().update_dataset_info_and_set_heads(opt, Dataset)
    print(opt)

    # 更新torch后，发现必须在import torch之前设置好该环境变量
    os.environ['CUDA_VISIBLE_DEVICES'] = opt.gpus_str
    opt.device = torch.device('cuda' if opt.gpus[0] >= 0 else 'cpu')

    print('Creating model...')
    model = create_model(opt.arch, opt.heads, opt.head_conv, opt.gray_img)
    print(torch.cuda.current_device())

    logger = Logger(opt)

    # 将模型呈现在tensorboardX TODO
    # fake_img = torch.randn([3, 1 if opt.gray_img==1 else 3, opt.input_h, opt.input_w])
    # logger.writer.add_graph(model, (fake_img,))

    optimizer = torch.optim.Adam(model.parameters(), opt.lr)
    start_epoch = 0
    # 若load_model项不为空，则加载模型权值
    if opt.load_model != '':
        model, optimizer, start_epoch = load_model(
            model, opt.load_model, optimizer, opt.resume, opt.lr, opt.lr_step)

    Trainer = train_factory[opt.task]
    trainer = Trainer(opt, model, optimizer)
    trainer.set_device(opt.gpus, opt.chunk_sizes, opt.device)

    print('Setting up data...')
    val_loader = torch.utils.data.DataLoader(
        # 实例化
        Dataset(opt, 'val'),
        batch_size=1,
        shuffle=False,
        num_workers=1,
        pin_memory=True
    )

    if opt.test:
        _, preds = trainer.val(0, val_loader)
        val_loader.dataset.run_eval(preds, opt.save_dir)
        return

    train_loader = torch.utils.data.DataLoader(
        Dataset(opt, 'train'),
        batch_size=opt.batch_size,
        shuffle=True,
        num_workers=opt.num_workers,
        pin_memory=True,
        drop_last=True
    )

    print('Starting training...\nCurrent Training Epoch is:{}'.format(start_epoch))
    print(torch.cuda.current_device())
    if start_epoch >= opt.num_epochs:
        print("--------!--------\nERROR! Please change the arg: '--num_epoch' above {}".format(start_epoch))
    best = 1e10
    for epoch in range(start_epoch + 1, opt.num_epochs + 1):
        start_time = time()

        mark = epoch if opt.save_all else 'last'
        # 训练一个epoch
        log_dict_train, _ = trainer.train(epoch, train_loader)

        time1 = time()
        print(f'time to train a epoch: {time1-start_time} s')

        logger.write('epoch: {} |'.format(epoch))
        for k, v in log_dict_train.items():
            # scalar_summary是记录到tensorboardX文件中，可可视化呈现
            logger.scalar_summary('train_{}'.format(k), v, epoch)
            # write是写入log.txt日志文件
            logger.write('{} {:8f} | '.format(k, v))

        time2 = time()
        print(f'time to write a log: {time2-time1} s')

        # 每个epoch都会保存一次权值。每val_intervals个epoch，在验证集做一次评估
        if opt.val_intervals > 0 and epoch % opt.val_intervals == 0:
            save_model(os.path.join(opt.save_dir, 'model_{}.pth'.format(mark)),
                       epoch, model, optimizer)
            with torch.no_grad():
                # 评估，日志中记录验证集损失
                log_dict_val, preds = trainer.val(epoch, val_loader)
                # 增加求mAP逻辑 TODO

            for k, v in log_dict_val.items():
                logger.scalar_summary('val_{}'.format(k), v, epoch)
                logger.write('{} {:8f} | '.format(k, v))
            # 如果评估结果比历史最优还要好，则将本次设为最优，保存本次的权值。注意：这里保存的内容只包含 epoch 和 model
            if log_dict_val[opt.metric] < best:
                best = log_dict_val[opt.metric]
                save_model(os.path.join(opt.save_dir, 'model_best.pth'),
                           epoch, model)
        else:
            # 保存 epoch、model、优化器
            save_model(os.path.join(opt.save_dir, 'model_last.pth'),
                       epoch, model, optimizer)
        logger.write('\n')

        time3 = time()
        print(f'time to save a model: {time3-time2} s')

        # 如果下个epoch要更换学习率，则也保存
        if epoch in opt.lr_step:
            save_model(os.path.join(opt.save_dir, 'model_{}.pth'.format(epoch)),
                       epoch, model, optimizer)
            lr = opt.lr * (0.1 ** (opt.lr_step.index(epoch) + 1))
            print('Drop LR to', lr)
            for param_group in optimizer.param_groups:
                param_group['lr'] = lr
    logger.close()


if __name__ == '__main__':
    opt = opts().parse()
    main(opt)
