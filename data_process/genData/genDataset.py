#encoding:utf-8
import random
import pathlib
import os
import sys
import shutil
from labelme2coco import labelme2coco

# from pycocotools.coco import COCO
# import numpy as np
# import skimage.io as io
# import matplotlib.pyplot as plt
# import pylab
from tqdm import tqdm
cut_ratio = 0.95 # 训练集占总数据集的比例
basePath = '/data1/shanglin/data'
genPath = '/data1/shanglin/data_process/cocodata1206'
if not os.path.exists(genPath):
    os.mkdir(genPath)
# 获取路径
# basePath = pathlib.Path(".")
# dataDir = pathlib.Path(os.path.join(basePath,"总891张_大类"))
dataDir = pathlib.Path(os.path.join(basePath,'data-1206-6150b-general-nowzl-enh'))


# labelFilePath = os.path.abspath("labels.txt")
labelFilePath = "/data1/shanglin/data_process/genData/labels.txt"

# 遍历目录下所有的json后缀的文件，并记录其路径
# 注意：有的后缀名是大写
filesPath = sorted(list(dataDir.rglob('*.json')))
totalNum = len(filesPath)
print("共标记了%d张图片" % totalNum)

###################### 数据集划分为训练集和验证集
trainSet = []
valSet = []

index = random.sample(range(0,totalNum),round(totalNum*cut_ratio))
for x in range(0,totalNum):
    if x in index:
        trainSet.append(filesPath[x])
    else:
        valSet.append(filesPath[x])


trainSetDir = os.path.join(genPath, "trainSet")
valSetDir = os.path.join(genPath, "valSet")

if os.path.exists(trainSetDir):
    shutil.rmtree(trainSetDir)
if os.path.exists(valSetDir):
    shutil.rmtree(valSetDir)

os.mkdir(trainSetDir)
os.mkdir(valSetDir)

print("正在生成训练集")
for x in tqdm(trainSet):
    imgFileName = str(x)[:-4]+'png'
    shutil.copy(x, trainSetDir)
    shutil.copy(imgFileName, trainSetDir)
print("正在生成验证集")
for x in tqdm(valSet):
    imgFileName = str(x)[:-4]+'png'
    shutil.copy(x, valSetDir)
    shutil.copy(imgFileName, valSetDir)

######################## 转为coco格式

annoDir = os.path.join(genPath, "annotations") # /data/1sl/data/annotations
if os.path.exists(annoDir):
    shutil.rmtree(annoDir)
os.mkdir(annoDir)

print("正在将训练集转为coco格式")
labelme2coco(labelFilePath, trainSetDir, os.path.join(annoDir, "train.json"))
print("正在将验证集转为coco格式")
labelme2coco(labelFilePath, valSetDir, os.path.join(annoDir, "val.json"))














