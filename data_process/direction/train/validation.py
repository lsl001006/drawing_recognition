
import shutil
import math
import os
import cv2
import json
import torch.utils.data.dataloader
from tqdm import tqdm
import torchvision.transforms.functional as F
from pathlib import Path
import numpy as np
torch.backends.cudnn.enabled = True
torch.backends.cudnn.benchmark = True

img_path = '/data1/shanglin/data_process/direction/data/data1129/val/img'
label_path = '/data1/shanglin/data_process/direction/data/data1129/val/label'

def get_key_points(heatmap, height, width):
    # Get final heatmap
    heatmap = np.asarray(heatmap.cpu().data)
    
    key_points = [[] for i in range(5)]
    # Get k key points from heatmap6
    for i in range(len(heatmap)):
        x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
        a = [x, y]
        # print(x,y)
        # print(heatmap[i][x][y])
        key = []
        # Get the coordinate of key point in the heatmap (46, 46)
        while (a != [] and heatmap[i][x][y] > 0.2):
            key.append(a)
            for j in range(-2, 2):
                for z in range(-2, 2):
                    if x + j < 64 and y + z < 64:
                        heatmap[i][x + j][y + z] = 0
            x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
            a = [x, y]
            # Calculate the scale to fit original image
        scale_x = width / heatmap[i].shape[1]
        scale_y = height / heatmap[i].shape[0]
        for z in key:
            y = int(z[0] * scale_x)
            x = int(z[1] * scale_y)
            key_points[i].append([x, y])
    return key_points

def model_pre(image, model, model_size=(256, 256)):
    """
    根据模型进行预测
    """
    height, width, _ = image.shape
    image = np.asarray(image, dtype=np.float32)
    image = cv2.resize(image, model_size, interpolation=cv2.INTER_CUBIC)
    image -= image.mean()
    image = F.to_tensor(image)
    image = torch.unsqueeze(image, 0).cuda()
    model.eval()
    input_var = torch.autograd.Variable(image)
    with torch.no_grad():
        result = model(input_var)
        pre_img = result[0, 5, :, :, :]
        key_points = get_key_points(pre_img, height=height, width=width)
    return key_points

def posCheck(keypoints,label):
    """
    检查位置偏差
    return: reslist
    """
    global posnums
    global poslist
    for i in range(len(keypoints)):
        if len(keypoints[i]) > 0 and label["shapes"][i]["points"][0] != None:
            pred = keypoints[i][0]
            gt = label["shapes"][i]["points"][0]
            dist = math.sqrt((pred[0]-gt[0])**2+(pred[1]-gt[1])**2)
            if dist < 5:
                poslist[i]+=1
            posnums[i]+=1
       

def angleCheck(keypoints, label):
    """
    检查角度偏差
    return: anglelist
    """
    global anglelist
    global anglenums
    if len(keypoints[0]) != 0:
        # print(label["imagePath"])
        print(keypoints)
        for i in range(1,len(keypoints)):
            if len(keypoints[i]) != 0 and label["shapes"][i]["points"][0] != None:
                v_pred = np.array(keypoints[i][0]) - np.array(keypoints[0][0])
                v_gt =  np.array(label['shapes'][i]['points'][0]) - np.array(label['shapes'][0]['points'][0])
                len_vp = np.linalg.norm(v_pred) # 求向量长度
                len_vg = np.linalg.norm(v_gt)
                cos_theta = round(np.dot(v_pred,v_gt.T)/(len_vg*len_vp),2)
                theta = math.acos(cos_theta)
                angle = (theta/math.pi)*180
                print(f"i = {i} pred: {v_pred} gt: {v_gt} theta: {angle}")
                
                if angle < 5:
                    anglelist[i-1] += 1
                anglenums[i-1] += 1
                print(anglenums)
    

def drawImg(keypoints, dst):
    """
    将结果绘制到图上
    """
    # GBR = [yellow,red,Magenta,Green,Gray]
    GBR = [(0,255,255),(0,0,255),(255,0,255),(0,255,0),(105,105,105)]
    if keypoints is not None:
        for i in range(len(keypoints)):
            if len(keypoints[i]) > 0 and keypoints[i][0] != None:
                cv2.circle(img, (keypoints[i][0][0],keypoints[i][0][1]), 3, GBR[i], 4)
        cv2.imwrite(os.path.join(dst,file),img)

def sumlabels(files):
    """
    计算每种标签都出现了多少次
    [Ns,Ne,Ne-1,Ne-2,Ne-3]
    """
    labels = ['0','1','2','3','4']
    labeldict = dict.fromkeys(labels,0)
    for file in files:
        filename = os.path.splitext(file)[0]
        data = json.load(Path(os.path.join(label_path,filename+'.json')).open('r'))
        for shape in data["shapes"]:
            if shape["label"] in labels:
                labeldict[shape["label"]] += 1
    # print(labeldict)
    return labeldict

if __name__ == "__main__":
    label_nums = 5
    files = os.listdir(img_path)
    model_path = "/data1/shanglin/data_process/direction/exp/aug1201/model_best.pth"
    dst = "/data1/shanglin/data_process/direction/val_result1201-2"
    if not os.path.exists(dst):
        os.mkdir(dst)
    else:
        shutil.rmtree(dst)
        os.mkdir(dst)
    # gtnums = sumlabels(files)
    # resdict = {"Ns":0,"Ne":0,"Ne-1":0,"Ne-2":0,"Ne-3":0}
    anglenums = [0 for i in range(1,label_nums)]
    posnums = [0 for i in range(label_nums)]
    poslist = [0 for i in range(label_nums)] # 结果存放
    anglelist = [0 for i in range(1,label_nums)]
    model = torch.load(model_path).cuda()  # 模型加载
    for file in tqdm(files):
        img = cv2.imread(os.path.join(img_path,file))
        filename = os.path.splitext(file)[0]
        label = json.load(Path(os.path.join(label_path,filename+'.json')).open('r')) 
        keypoints = model_pre(img,model,model_size=(256,256))
        print(keypoints)
        # poslist为满足位置条件的1*label_nums向量，每一个元素代表Ns,Ne,S1,S2,S3点是否满足位置条件
        posCheck(keypoints,label)
        # print(poslist)
        # anglelist为满足角度条件的1*label_nums向量，每一个元素代表N,S1,S2,S3方向的向量是否满足角度条件
        angleCheck(keypoints, label) 
        drawImg(keypoints, dst)

    print("======================= result =======================")
    print(f"> 整体方向准确度 : {round(sum(anglelist)/sum(anglenums)*100,4)}%")
    print(f"> 整体位置准确度 : {round(sum(poslist)/sum(posnums)*100,4)}%")
    print(f'> Ns   [位置准确度]=> {round(poslist[0]/posnums[0]*100,4)}%')
    print(f'> Ne   [位置准确度]=> {round(poslist[1]/posnums[1]*100,4)}% [方向准确度]=> {round(anglelist[0]/anglenums[0]*100,4)}%')
    print(f'> Ne-1 [位置准确度]=> {round(poslist[2]/posnums[2]*100,4)}% [方向准确度]=> {round(anglelist[1]/anglenums[1]*100,4)}%')
    print(f'> Ne-2 [位置准确度]=> {round(poslist[3]/posnums[3]*100,4)}% [方向准确度]=> {round(anglelist[2]/anglenums[2]*100,4)}%')
    print(f'> Ne-3 [位置准确度]=> {round(poslist[4]/posnums[4]*100,4)}% [方向准确度]=> {round(anglelist[3]/anglenums[3]*100,4)}%')

