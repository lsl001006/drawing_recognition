#!/bin/bash

# 使用最新生成的coco1206数据集训练
python main.py ctdet --dataset draw3 --gpus 2 --save_all  \
    --batch_size 4 --exp_id 1206-Train-2 --num_epochs 200 --no_color_aug --val_intervals 10 \
    #--resume
    