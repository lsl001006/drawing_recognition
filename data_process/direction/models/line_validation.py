import torch
from torch import nn
from models.layers import Conv, Hourglass, Pool, Residual
from models.posenet import PoseNet, Merge, UnFlatten


from task.loss import HeatmapLoss

class LineVectorizer(nn.Module):
    def __init__(self, nstack, inp_dim, out_dim,oup_line, bn=False, increase=0):
        super(LineVectorizer,self).__init__()

        self.nstack = nstack
        self.inp_dim = inp_dim
        self.oup_line =oup_line
        self.oup_dim = out_dim

        self.pre = nn.Sequential(
            Conv(3, 64, 7, 2, bn=True, relu=True),
            Residual(64, 128),
            Pool(2, 2),
            Residual(128, 128),
            Residual(128, self.inp_dim)

        )  # 预处理部分

        self.hgs = nn.ModuleList([  # 每一个都叠四层
            nn.Sequential(
                Hourglass(4, inp_dim, bn, increase),
            ) for i in range(nstack)])

        self.features = nn.ModuleList([  # 残差块和单核卷积叠加，不改变通道数
            nn.Sequential(
                Residual(inp_dim, inp_dim),
                Conv(inp_dim, inp_dim, 9, bn=True, relu=True)
            ) for i in range(nstack)])

        self.outs = nn.ModuleList([Conv(inp_dim, out_dim, 9, relu=False, bn=False) for i in range(nstack)])
        self.merge_features = nn.ModuleList([Merge(inp_dim, inp_dim) for i in range(nstack - 1)])
        self.merge_preds = nn.ModuleList([Merge(out_dim, inp_dim) for i in range(nstack - 1)])

        #反卷积
        self.outlinefeature = nn.Conv2d(inp_dim, oup_line, bias=True, kernel_size=1, stride=1)
        self.relu = nn.ReLU(inplace=True)
        self.deconv1 = nn.ConvTranspose2d(oup_line, oup_line//2, kernel_size=5, stride=2, padding=2, output_padding=1, bias=False)
        self.bn1 = nn.BatchNorm2d(oup_line//2)
        self.deconv2 = nn.ConvTranspose2d(oup_line//2, oup_line//4, kernel_size=5, stride=2, padding=2, output_padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d( oup_line//4)
        self.conv2 = nn.Conv2d( oup_line//4, 1, kernel_size=5, stride=1, padding=2, bias=False)

        self.sigmoid = nn.Sigmoid()


    def forward(self, imgs):
        x = self.pre(imgs)  # 先经过预处理
        # x=self.up(x)
        combined_hm_preds = []

        for i in range(self.nstack):
            hg = self.hgs[i](x)
            feature = self.features[i](hg)
            preds = self.outs[i](feature)
            combined_hm_preds.append(preds)
            if i < self.nstack - 1:
                x = x + self.merge_preds[i](preds) + self.merge_features[i](feature)

        line_out_f=self.outlinefeature(hg)
        lineOut = self.relu(self.bn1(self.deconv1(line_out_f)))
        lineOut = self.relu(self.bn2(self.deconv2(lineOut)))
        lineOut = self.conv2(lineOut)

        return torch.stack(combined_hm_preds, 1),lineOut


    def calc_loss(self, combined_hm_preds, heatmaps):
        combined_loss = []
        for i in range(self.nstack):
            combined_loss.append(self.heatmapLoss(combined_hm_preds[0][:, i], heatmaps))
        combined_loss = torch.stack(combined_loss, dim=1)
        return combined_loss

