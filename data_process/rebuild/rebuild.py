
from PIL import Image, ImageDraw, ImageEnhance,ImageFont
import cv2
import json
from pathlib import Path
from tqdm import trange
import os
from math import ceil
from validation import model_pre
os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import torch



################### IMPORTANT ########################
## rebuild.py 图纸复原重建工具
## 主要用途：用于对预测结果进行复原（原图复原与零件库复原）               
## 关联文件：results_stage4.json
## 依赖环境：direction, python 3.6.13                   
## 关联程序：modififyres.py, validation.py                         
## 注意：                                 
######################################################

class_name = [
      'a1-1', 'a1-2', 'a1-3', 'a1-4', 'a3', 'a4', 'an', 'b1', 'c1', 'd1', 'g1', 'h1', 'l1', 'n1', 'p1-1',
      'p1-2', 's1', 't1', 't10', 't3-l', 't3-m', 't3-r', 't7', 't9', 'tn', 'v1','va1', 'va3', 'va4', 'van',
      'vb1', 'vp1-1', 'vp1-2', 'vp1-3', 'vp1-4', 'vp1-5', 'vp1-6', 'vp2-1', 'vp2-2', 'vp2-3', 'vpn', 'vs1',
      'vt1', 'vt10-1', 'vt10-2', 'vt10-n', 'vt3', 'vt4', 'vt7-1', 'vt7-2', 'vt9', 'vtn', 'z1', 'x1', 'k1',
      'x2', 'btl', 'tl', 'sm', 'wz1', 'wz1-l', 'wz2', 'wz3', 'wz3-l', 'wz4','j1','xtl','wztl','wlb'
]


fontsize = 40
font = ImageFont.truetype('/usr/share/fonts/opentype/noto/NotoSansCJK-Black.ttc',
                        fontsize,encoding="utf-8")


def WZPredict(ocr_result):
    """
    用于进行文字语义推断，根据残缺文字推断完整内容
    待补充 FIXME
    """
    return ocr_result

def n1_rebuild(n1_img, model_path):
    """
    识别原图中n1里面的5个点，并在图里画出来
    """
    
    n1_img.save(os.path.join(src_dir, "n1_tmp.png"))
    w,h = n1_img.size[0],n1_img.size[1]
    model = torch.load(model_path).cuda()  # 模型加载
    n1_img = cv2.imread(os.path.join(src_dir, "n1_tmp.png"))
    x_ratio = w/256
    y_ratio = h/256
    n1_img_2 = cv2.resize(n1_img, (256,256), interpolation=cv2.INTER_CUBIC)
    keypoints = model_pre(n1_img_2,model,model_size=(256,256))
    res = [[[]] for i in range(len(keypoints))]
    # print(keypoints)
    # drawImg(keypoints, src_dir, n1_img_2, "n1_pre_tmp.png")
    for i in range(len(keypoints)):
        if keypoints != None and keypoints[i] != None:
            if  len(keypoints[i]) > 0:
                res[i][0].append(ceil(keypoints[i][0][0]*x_ratio))
                res[i][0].append(ceil(keypoints[i][0][1]*y_ratio))
    # print(res)
    # drawImg(keypoints, src_dir, n1_img, "n1img.png")

    return res # keypoints的结构

def draw_n1(x, y, keypoints, draw_lib):
    """
    绘制关键点
    """
    r = 5 # 关键点的半径设为5
    for point in keypoints:
        if point[0] != None:
            pointX = x+point[0][0]
            pointY = y+point[0][1]
            # 由于点太小了，所以现在只能先画圆
            draw_lib.chord([(pointX-r,pointY-r),(pointX+r,pointY+r)],start = 0, end = 360, fill ="#FF0000")


def rebuild_with_original_img(result, img):
    """
    通过类别来切出小图，将n1排除在外
    返回
    """
    canvas_ori = Image.new("L", (img.size[0], img.size[1]), 255)
    for i in trange(len(result)):
        category_id = result[i]['category_id']
        category = class_name[category_id - 1]
        if result[i]['score'] > 0.4:
            x = int(result[i]['bbox'][0])
            y = int(result[i]['bbox'][1])
            w = ceil(result[i]['bbox'][2])
            h = ceil(result[i]['bbox'][3])
            # 根据预测结果把小图抠出来，并进行图像增强
            original_img = img.crop((x,y,x+w,y+h))
            original_img = ImageEnhance.Contrast(original_img).enhance(15)
            original_img_enhanced = ImageEnhance.Sharpness(original_img).enhance(5)
            original_img_resized = original_img_enhanced.resize((w,h), Image.ANTIALIAS)
            # 绘制于画布上
            canvas_ori.paste(original_img_resized, (x,y))
    canvas_ori.save(os.path.join(output_dir,"from_ori",pngName))


def rebuild_with_component_library(result, img, model_path):
    """
    在原图预测出的框内填充模型库的图片
    """
    canvas_lib = Image.new('L',(img.size[0], img.size[1]), 255)
    draw_lib = ImageDraw.Draw(canvas_lib)
    wzlabels = ["wz3","sm","v1","h1","d1"]
    exceptlabels = wzlabels+["n1"]
    for i in trange(len(result)):
        category_id = result[i]['category_id']
        category = class_name[category_id - 1]
        if category+'.png' not in component_name:
            continue
        if result[i]['score'] > 0.4:
            x = int(result[i]['bbox'][0])
            y = int(result[i]['bbox'][1])
            w = ceil(result[i]['bbox'][2])
            h = ceil(result[i]['bbox'][3])
            if category not in exceptlabels:
                component_img = Image.open(os.path.join(lib_path, category + '.png'))
                component_img = component_img.resize((w, h), Image.ANTIALIAS)
                canvas_lib.paste(component_img, (x, y))
            if category in wzlabels and "WzInfo" in result[i].keys():
                ocr_result = result[i]["WzInfo"]
                words = WZPredict(ocr_result)
                line_height = 0
                for j in range(len(words)):
                    draw_lib.text((x,y+line_height), words[j], font=font)
                    line_height += fontsize
            if category == "n1":
                n1_img = img.crop((x,y,x+w,y+h))
                n1_img = ImageEnhance.Contrast(n1_img).enhance(15)
                n1_enhanced = ImageEnhance.Sharpness(n1_img).enhance(5)
                n1_img_final = n1_enhanced.resize((w,h), Image.ANTIALIAS)
                keypoints = n1_rebuild(n1_img_final, model_path)
                # 将n1原图也画在图上来证明点的准确性
                n1_img_final = ImageEnhance.Contrast(n1_img_final).enhance(0.1)
                canvas_lib.paste(n1_img_final, (x,y))
                draw_n1(x, y, keypoints, draw_lib)

    canvas_lib.save(os.path.join(output_dir,"from_lib",pngName))
            
        
         
if __name__ == "__main__":
    # 工作目录
    src_dir = "/data1/shanglin/data_process/rebuild"
    # 输出路径
    output_dir = os.path.join(src_dir,"output_pics")
    # 模型路径
    model_path = "/data1/shanglin/data_process/direction/exp/aug1201/model_best.pth"
    # 零件库路径
    lib_path = os.path.join(src_dir,'零件库v2')
    # 读入 处理后的 results.json的路径
    resJson_path = os.path.join(src_dir,"resJson","results_stage4.json")
    data = json.load(Path(resJson_path).open('rb'))

    result = data[48] # 使用其中的哪一张图片
    pngName = result[0]["file_name"]+".png"
    print(pngName)
    img_dir = os.path.join(src_dir,"input_pics",pngName) 
    img = Image.open(img_dir)
    component_name = os.listdir(lib_path)
    
    # 使用原图重建
    rebuild_with_original_img(result, img)
    # 使用零件库重建
    rebuild_with_component_library(result, img, model_path)
    # 把原图存在ori_imgs文件夹下
    img.save(os.path.join(output_dir,"ori_imgs",pngName))