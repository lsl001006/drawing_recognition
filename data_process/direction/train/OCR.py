from paddleocr import PaddleOCR, draw_ocr
from PIL import Image
from PIL import ImageEnhance
import os

os.environ["CUDA_VISIBLE_DEVICES"] = "1"

# ocr = PaddleOCR(use_angle_cls=False, lang="ch")  # need to run only once to download and load model into memory
ocr = PaddleOCR(
    use_angle_cls=False,
    use_gpu=True,
    cls_model_dir = '/home/guojie/.paddleocr/2.3.0.2/ocr/cls/ch',
    det_model_dir = '/home/guojie/.paddleocr/2.3.0.2/ocr/det/ch',
    rec_model_dir = '/home/guojie/.paddleocr/2.3.0.2/ocr/rec/ch'
)

# img = img = Image.open('/data1/guojie/direction/shuoming2.png')
# enh_con = ImageEnhance.Contrast(img)  
# contrast = 15
# img_contrasted = enh_con.enhance(contrast) 
img_path = '/data1/guojie/direction/shuoming2.png'
# img_contrasted.save(img_path)
# img_path = '/data1/guojie/direction/shuoming2_enhance.png'

result = ocr.ocr(img_path, cls=False)
# text = ''
text = []
for line in result:
    text.append(line[-1][0])
# 显示结果
# from PIL import Image

# image = Image.open(img_path).convert('RGB')
# boxes = [line[0] for line in result]
# txts = [line[1][0] for line in result]
# scores = [line[1][1] for line in result]
# im_show = draw_ocr(image, boxes, txts, scores, font_path='./fonts/simfang.ttf')
# im_show = Image.fromarray(im_show)
# im_show.save('result.jpg')
# 利用逻辑判断纠正错误，需要继续优化
inds = []
numbers = '0123456789'
for i in range(len(text)):
    if text[i] == ':':
        if text[i-1] not in numbers or text[i+1] not in numbers:
            text[i]=','
for i in range(len(text)):
    if text[i] == '.':
        if text[i-1] not in numbers or text[i+1] not in numbers:
            text[i]=','
for ind, word in enumerate(text):
    if word == '.' :
        if text[ind-1]  in numbers and text[ind+1]  in numbers:
            continue
        else:
            inds.append(ind)
structured_text = []
#分出条目
# print(numbers)
for i in range(len(inds)):
    if i != len(inds)-1:
        subtext = text[(inds[i]-1):(inds[i+1]-1)]
    else:
        subtext = text[(inds[i]-1):]
    # print(subtext)
    structured_text.append(subtext)
print(text)
print(structured_text)