import os
import json
import re
import cv2
import pdb
import pynvml
from tqdm import tqdm
from math import sqrt, ceil
from paddleocr import PaddleOCR
from validation import drawImg, model_pre
from PIL import Image, ImageDraw, ImageEnhance, ImageOps

os.environ["CUDA_VISIBLE_DEVICES"] = "3"
import torch

##################### IMPORTANT #########################
## results.json后处理程序                               
## 主要功能：对results.json标上mask_id,WzInfo,match_list 
##          和n1_cords，并按image_id进行切分             
## 依赖环境：direction, python3.6.13                     
## 依赖库：paddleocr, PIL, torch                        
## 关联程序：rebuild.py, validation.py
## 输出文件夹：rebuild/modified_results
## 注意：n1关键点预测模型只能接受来自白底色图片的输入                 
#########################################################


cnt = 0 # 临时图片计数用
ocr = PaddleOCR(
    use_angle_cls=False,
    use_gpu=True,
    cls_model_dir = '/data1/shanglin/data_process/.paddleocr/2.3.0.2/ocr/cls/ch',
    det_model_dir = '/data1/shanglin/data_process/.paddleocr/2.3.0.2/ocr/det/ch',
    rec_model_dir = '/data1/shanglin/data_process/.paddleocr/2.3.0.2/ocr/rec/ch'
    )


def paddle_ocr(img_path):
    """
    paddle_ocr功能函数,调用paddle_ocr进行文字识别
    :param img_path:输入图片路径
    """
    result = ocr.ocr(img_path, cls=False)
    text = []
    for line in result:
        text.append(line[-1][0])
    return text

def n1_rebuild(n1_img, model_path):
    """
    识别原图中n1里面的5个点，并在图里画出来
    """
    
    n1_img.save(os.path.join(tmp_dir, "n1_tmp.png"))
    w,h = n1_img.size[0],n1_img.size[1]
    # 模型加载
    model = torch.load(model_path).cuda()
    n1_img = cv2.imread(os.path.join(tmp_dir, "n1_tmp.png"))
    x_ratio = w/256
    y_ratio = h/256
    n1_img_2 = cv2.resize(n1_img, (256,256), interpolation=cv2.INTER_CUBIC)
    keypoints = model_pre(n1_img_2,model,model_size=(256,256))
    res = [[[]] for i in range(len(keypoints))]
    # print(keypoints)
    # drawImg(keypoints, src_dir, n1_img_2, "n1_pre_tmp.png")
    for i in range(len(keypoints)):
        if keypoints != None and keypoints[i] != None:
            if  len(keypoints[i]) > 0:
                res[i][0].append(ceil(keypoints[i][0][0]*x_ratio))
                res[i][0].append(ceil(keypoints[i][0][1]*y_ratio))
    # print(res)
    # drawImg(keypoints, src_dir, n1_img, "n1img.png")
    return res # keypoints的结构

def distance(x1,y1,x2,y2):
    """
    计算欧式距离
    """
    return sqrt((x1-x2)**2+(y1-y2)**2)

def genImageInfo(pointlh, pointrd, img):
    """
    用于得到图所对应文字信息
    :param pointlh: 左上角坐标
    :param pointrd: 右下角坐标
    :param img: PIL打开的图片
    """
    global cnt
    img2 = img.crop((pointlh[0], pointlh[1], pointrd[0], pointrd[1]))
    img2 = ImageEnhance.Sharpness(img2).enhance(3)
    img2 = ImageEnhance.Contrast(img2).enhance(15)
    img_path = os.path.join(dst, "img" + str(cnt) + ".png")
    img2.save(img_path)
    cnt += 1
    ocr_result = paddle_ocr(img_path)
    os.remove(img_path) #删除临时图片
    return ocr_result

def genLbdict(lbpath):
    """
    生成标签-序号对应的字典
    :param lbpath: 标签文件labels.txt路径
    """
    lbdict = {}
    with open(lbpath,'r',encoding='utf-8') as f:
        lblist = f.readlines()
    for i,each in enumerate(lblist):
        tmp = each.split(' ')
        tmp2 = re.sub('\n','',tmp[1])
        lbdict[tmp2] = i+1
    return lbdict

def AddMaskIds(data):
    """
    为results.json中的每一张识别的图纸各自加上
    其内部图纸块的编号
    """
    print("----"*6+" ID PROCESS "+"----"*6)
    data = DataSplit(data)
    for i in tqdm(range(len(data))):
        for j in range(len(data[i])):
            data[i][j]["mask_id"] = j
    with open(os.path.join(dst,"results_stage2.json"),'w',encoding="utf-8") as f:
        json.dump(data,f,ensure_ascii=False,indent=4)
    return data

def centerPoint(bbox):
    """
    给出bbox的中心点坐标
    """
    x = bbox[0]+1/2*bbox[2]
    y = bbox[1]+1/2*bbox[3]
    return [x,y]

def DataById(data,i):
    """
    从results.json中提取出来某一张图纸的所有信息
    """
    data_i = []
    for each in data:
        if each["image_id"] == i:
            data_i.append(each)
    return data_i

def DataSplit(data):
    """
    把各个图的data从results.json中提取出来
    """
    data_split = []
    curr_img_id = 0
    for i in tqdm(range(len(data))):
        pre_img_id = curr_img_id
        curr_img_id = data[i]["image_id"]
        if curr_img_id == pre_img_id:
            continue
        else:
            data_split.append(DataById(data,curr_img_id))

    return data_split

def AddTxt(data, lbdict, datapath, dst):
    """
    为results.json的wz标签添加OCR信息
    :param data: results.json的导入数据
    :param lbdict: 标签-序号对应的字典
    :param datapath: 图片路径
    :param dst: 目标文件路径
    """
    ocr_labels = ['wz1','wz3','sm','v1','d1','wztl','h1'] #待ocr识别标签类型
    ocr_cat_ids = [lbdict[p] for p in ocr_labels]
    print("----"*6+" OCR PROCESS "+"----"*6)

    for i in tqdm(range(len(data))):
        img_path = os.path.join(datapath,data[i][0]['file_name']+'.png')
        img = Image.open(img_path)
        # 对于每一张图片
        for j in range(len(data[i])):   
            if data[i][j]["category_id"] in ocr_cat_ids and data[i][j]["score"]>=0.3:
                
                x = data[i][j]["bbox"][0]
                y = data[i][j]['bbox'][1]
                w = data[i][j]['bbox'][2]
                h = data[i][j]['bbox'][3]
                lh = [x,y]
                rd = [x+w,y+h]
                # if data[i][j]["category_id"] == 68:
                #     img_wztl = img.crop((x,y,rd[0],rd[1]))
                #     img_wztl.save("/data1/shanglin/data_process/rebuild/modified_results/tmp/tmp_wz.png")
                #     pdb.set_trace()
                    
                txt = genImageInfo(lh,rd,img)
                print('--------')
                print(data[i][j]["category_id"])
                print(txt)
                print("--------")
                # print(f"-------------- {data[i][j]["category_id"]} {txt} --------------")
                data[i][j]['WzInfo'] = txt
    # 存入json文件
    with open(os.path.join(dst,"results_stage3.json"),'w',encoding='utf-8') as f:
        json.dump(data,f,ensure_ascii=False,indent=4)
    return data


def MatchWZ(data, mask_id, lbdict, scale=50):
    """
    对单个文字标签在本图内进行搜索匹配
    :param data: results.json截出来单张图片的数据
    :param mask_id: 图纸块编号
    :param lbdict: 标签-序号对应的字典
    :param scale: 搜索范围，默认50
    """
    res = []
    # 非匹配标签
    dont_match = ['wz1','wz3','wz1-l','wz3-l','wz2','wz4','d1','h1',
                  'v1','k1','j1','wztl']
    dm_cat_ids = [lbdict[p] for p in dont_match]
    wz_data = data[mask_id]
    ct_wz = centerPoint(wz_data["bbox"]) # 给出文字标签的中心点坐标[x,y]
    for i in range(len(data)): 
        # 如果搜索到的标签 在同一张图内 且 不是非匹配标签
        if data[i]["category_id"] not in dm_cat_ids and data[i]["score"]>0.3:
            ct_this = centerPoint(data[i]['bbox'])
            # 如果两个中心点距离小于阈值scale
            dis = distance(ct_wz[0],ct_wz[1],ct_this[0],ct_this[1])
            if dis < scale:
                # 通过category_id反推key
                label = list(lbdict.keys())[list(lbdict.values()).index(data[i]["category_id"])]
                res.append({"pair_id":data[i]["mask_id"],"label":label,"distance":dis})
    if len(res)>=1:
        res.sort(key=lambda x: x["distance"], reverse=False) # 按距离从小到大排序
        return res
    else:
        return None
    

def AddMatch(data, dst, lbdict, scale=50, stride=100):
    """
    为results.json增加图文匹配
    """
    ori_scale = scale
    wzlabel = ['wz1','wz3']
    wz_cat_ids =  [lbdict[p] for p in wzlabel]
    print("----"*6+" MATCH PROCESS "+"----"*6)

    for i in tqdm(range(len(data))):
        # 将该图片打开
        img = Image.open(os.path.join(dataPath,data[i][0]['file_name']+'.png'))
        draw = ImageDraw.Draw(img)
        # 对每张图搜索
        for j in range(len(data[i])):
            # 如果该标签是文字标签，且score>0.3,并且含有WzInfo键，则进行搜索
            if data[i][j]["category_id"] in wz_cat_ids and data[i][j]["score"]>0.3 and "WzInfo" in data[i][j].keys():
                mask_id = data[i][j]["mask_id"]
                match_result = None
                while not match_result: # run until find a match
                    match_result = MatchWZ(data[i], mask_id, lbdict, scale)
                    scale += stride
                    if scale > 800: break
                scale = ori_scale
                data[i][j]["match_list"] = match_result
                """以下进行原图连线验证match_list是否准确"""
                if len(match_result)>0:
                    ct_wz = centerPoint(data[i][j]["bbox"]) # 给出文字标签的中心点坐标[x,y]
                    for dic in match_result:
                        # 对应匹配标签的中心点坐标
                        ct_lb = centerPoint(data[i][dic["pair_id"]]['bbox'])
                        draw.line((ct_wz[0],ct_wz[1],ct_lb[0],ct_lb[1]),fill="yellow",width=5)
        img = ImageEnhance.Brightness(img).enhance(1.5)
        img = ImageEnhance.Contrast(img).enhance(15)
        img.save(os.path.join(dst,'wz_match_imgs',data[i][0]['file_name']+'.png'))
    # 存入json文件
    with open(os.path.join(dst, "results_stage4.json"),'w',encoding='utf-8') as f:
        json.dump(data,f,ensure_ascii=False,indent=4)
    return data

def Addn1cords(data, dst_n1, lbdict, dataPath, isblack):
    """
    加入n1的各点坐标
    """
    for imgdata in tqdm(data):
        img = Image.open(os.path.join(dataPath, imgdata[0]["file_name"]+".png"))
        for i in range(len(imgdata)):
            if imgdata[i]["category_id"] == lbdict["n1"] and\
                imgdata[i]["score"] > 0.4:
                x = int(imgdata[i]['bbox'][0])
                y = int(imgdata[i]['bbox'][1])
                w = ceil(imgdata[i]['bbox'][2])
                h = ceil(imgdata[i]['bbox'][3])
                
                if isblack:
                    img = ImageOps.invert(img)
                n1_img = img.crop((x,y,x+w,y+h))
                n1_img = ImageEnhance.Contrast(n1_img).enhance(15)
                n1_enhanced = ImageEnhance.Sharpness(n1_img).enhance(5)
                n1_img_final = n1_enhanced.resize((w,h), Image.ANTIALIAS)
                keypoints = n1_rebuild(n1_img_final, model_path)
                # print(keypoints)
                imgdata[i]["n1_cords"] = keypoints
                n1_img_final.save(os.path.join(dst, "n1_tmp.png"))
                n1_img_final = cv2.imread(os.path.join(dst, "n1_tmp.png"))
                drawImg(keypoints, dst_n1, n1_img_final, imgdata[0]["file_name"]+".png")
    with open(os.path.join(dst, "results_stage5.json"), "w", encoding='utf-8') as f:
        json.dump(data, f, ensure_ascii=False, indent=4)
    return data



    
if __name__ == "__main__":
    # 传入图片是否为黑色背景
    isblack = True
    # results.json存放路径
    path = "/data1/shanglin/Centernet/exp/ctdet/1126-Test-1/results.json"
    # 各类文件生成路径
    dst = "/data1/shanglin/data_process/rebuild/modified_results"
    # n1验证图生成路径
    dst_n1 = "/data1/shanglin/data_process/rebuild/modified_results/n1_imgs"
    # 临时文件存放地址
    tmp_dir = "/data1/shanglin/data_process/rebuild/modified_results/tmp"
    # labels.txt存放路径
    lbpath = "/data1/shanglin/data_process/rebuild/labels.txt"
    # 对应results.json中所有图片的存放位置
    dataPath = "/data1/shanglin/data_process/cocodata_for_test/valSet"
    # n1关键点识别模型存放位置
    model_path = "/data1/shanglin/data_process/direction/exp/aug1201/model_best.pth"

    if not os.path.exists(dst):
        os.mkdir(dst)
    
    # 加载标签-序号对应字典
    lbdict = genLbdict(lbpath)

    # 加载原始results.json数据
    with open(path,'rb') as f:
        data = json.load(f)

    # 为results.json加入图块编号,生成results_stage2.json
    data2 = AddMaskIds(data)

    # 为results.json加入OCR文本识别结果,生成results_stage3.json
    data3 = AddTxt(data2, lbdict, dataPath, dst)

    # 为results.json加入匹配mask_id,生成results_stage4.json
    data4 = AddMatch(data3, dst, lbdict, scale=100, stride=50)

    # with open(os.path.join(dst, "results_stage4.json"), 'rb') as f:
    #     data4 = json.load(f)

    # 为results.json加入n1的预测结果，生成results_stage5.json
    data5 = Addn1cords(data4, dst_n1, lbdict, dataPath, isblack)