import cv2
import random
import numbers
import numpy as np


#
# def random_flip(img, mode=1):
#     """
#     随机翻转
#     :param img:
#     :param model: 1=水平翻转 / 0=垂直 / -1=水平垂直
#     :return:
#     """
#     assert mode in (0, 1, -1), "mode is not right"
#     flip = np.random.choice(2) * 2 - 1  # -1 / 1
#     if mode == 1:
#         img = img[:, ::flip, :]
#     elif mode == 0:
#         img = img[::flip, :, :]
#     elif mode == -1:
#         img = img[::flip, ::flip, :]
#
#     return img


def random_contrast(im, lower=0.5, upper=1.5):
    if random.random() < 0.6:
        alpha = random.uniform(lower, upper)
        im *= alpha
        im = im.clip(min=0, max=255)
    return im


def random_bright(im, delta=32):
    if random.random() < 0.6:
        delta = random.uniform(-delta, delta)
        im += delta
        im = im.clip(min=0, max=255)
    return im


def move_img(img, key_points, x_d, y_d):
    if random.random() < 0.4:
        M = np.float32([[1, 0, x_d], [0, 1, y_d]])
        h, w, n = img.shape
        # 第一个参数为原图像，第二个参数为移动矩阵，可以自定义，第三个参数为输出图像大小
        img = cv2.warpAffine(img, M, (h, w))
        for i in range(len(key_points)):
            if key_points[i]!=[]:
                for j in range(len(key_points[i])):
                    key_points[i][j][0] = key_points[i][j][0] + x_d
                    key_points[i][j][1] = key_points[i][j][1] + y_d
                    if key_points[i][j][0]>255:
                        key_points[i][j][0]=255
                    if key_points[i][j][1]>255:
                        key_points[i][j][1]=255
    return img, key_points


def resize_image_points(image, key_points, ratio):
    num = len(key_points)
    for i in range(num):
        if key_points[i]!=None:
            for j in range(len(key_points[i])):
                key_points[i][j][0] *= ratio[0]
                key_points[i][j][1] *= ratio[1]
    image = random_contrast(im=image)
    image = random_bright(im=image)
    image,key_points= move_img(img=image,key_points=key_points,x_d=5,y_d=5)
    return cv2.resize(image, (0, 0), fx=ratio[0], fy=ratio[1]),key_points


class Mouse_resized(object):
    def __init__(self, scale_min=0.3, scale_max=1.1):
        self.scale_min = scale_min
        self.scale_max = scale_max

    def __call__(self, image, key_points,scale):
        return resize_image_points(image, key_points, scale)

# class RandomResized(object):
# 	"""Randomly resized given numpy array"""
#
# 	def __init__(self, scale_min=0.3, scale_max=1.1):
# 		self.scale_min = scale_min
# 		self.scale_max = scale_max
#
# 	def __call__(self, image, key_points, center_points, scale):
# 		"""
# 		Randomly resized given numpy array
# 		:param image: The image to be resized.
# 		:param key_points: The key points to be resized.
# 		:param center_points: The center points to be resized.
# 		:param scale: The scale of each image, representing the main part, which is calculated at line 63 of gen_data.py.
# 		:return: Randomly resize image, key points and center points.
# 		"""
# 		random_scale = random.uniform(self.scale_min, self.scale_max)
# 		ratio = random_scale / scale
#
# 		return resize_image_points(image, key_points, center_points, ratio)
#
#
# class TestResized(object):
# 	def __init__(self, size):
# 		self.size = (size, size)
#
# 	def __call__(self, image, key_points, center_points):
# 		height, width, _ = image.shape
# 		ratio = ((self.size[0] * 1.0) / width, (self.size[1] * 1.0) / height)
#
# 		return resize_image_points(image, key_points, center_points, ratio)
#
#
# def crop(image, key_points, center_points, offset_left, offset_up, new_height, new_width):
# 	num = len(key_points)
# 	for i in range(num):
# 		if key_points[i][2] == 0:
# 			# The key points is blocked
# 			continue
# 		# Translate key points
# 		key_points[i][0] -= offset_left
# 		key_points[i][1] -= offset_up
#
# 	# Translate center points
# 	center_points[0] -= offset_left
# 	center_points[1] -= offset_up
#
# 	# Get the width and height of original image
# 	ori_height, ori_width, _ = image.shape
#
# 	new_image = np.empty((new_width, new_height, 3), dtype=np.float32)
# 	new_image.fill(128)
#
# 	# The coordinates of new image
# 	start_x = 0
# 	end_x = new_width
# 	start_y = 0
# 	end_y = new_height
#
# 	# The coordinates of original image
# 	ori_start_x = offset_left
# 	ori_end_x = ori_start_x + new_width
# 	ori_start_y = offset_up
# 	ori_end_y = ori_start_y + new_height
#
# 	if offset_left < 0:
# 		# start_x should be added, because center points and key points are added
# 		start_x = -offset_left
# 		ori_start_x = 0
# 	if ori_end_x > ori_width:
# 		end_x = ori_width - offset_left
# 		ori_end_x = ori_width
#
# 	if offset_up < 0:
# 		start_y = -offset_up
# 		ori_start_y = 0
# 	if ori_end_y > ori_height:
# 		end_y = ori_height - offset_up
# 		ori_end_y = ori_height
#
# 	new_image[start_y: end_y, start_x: end_x, :] = image[ori_start_y: ori_end_y, ori_start_x: ori_end_x, :].copy()
#
# 	return new_image, key_points, center_points
#
#
# class RandomCrop(object):
# 	def __init__(self, size):
# 		self.size = (size, size)  # (368, 368)
#
# 	def __call__(self, image, key_points, center_points):
# 		x_offset = random.randint(-5, 5)
# 		y_offset = random.randint(-5, 5)
# 		center_x = center_points[0] + x_offset
# 		center_y = center_points[1] + y_offset
#
# 		offset_left = int(round(center_x - self.size[0] / 2))
# 		offset_up = int(round(center_y - self.size[1] / 2))
#
# 		return crop(image, key_points, center_points, offset_left, offset_up, self.size[0], self.size[1])
#
#
# class Compose(object):
# 	def __init__(self, transforms):
# 		self.transforms = transforms
#
# 	def __call__(self, img, kpt, center, scale=None):
# 		for t in self.transforms:
# 			if isinstance(t, RandomResized):
# 				img, kpt, center = t(img, kpt, center, scale)
# 			else:
# 				img, kpt, center = t(img, kpt, center)
#
# 		return img, kpt, center
