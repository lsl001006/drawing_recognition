import shutil
import numpy as np
import os
import cv2
import json
from pathlib import Path
import pdb
from tqdm import tqdm
from labelme import utils
import random
from PIL import Image
from PIL import ImageEnhance

def n1_exists(data_old):
    """
    检查是否有n1标签，没有则返回False
    """
    for item in data_old['shapes']:
        if item['label'] == 'n1':
            return True
    return False

def get_small_img(img,data_old):
    """
    把建北指北的小图从原图中抠出来
    """
    margin = 5 # 抠图的时候留5个像素点的余地
    shapes_old = data_old['shapes']
    L = []
    K = []
    for item in shapes_old:
        if item['label'] == 'n1':
            x1 = int(item['points'][0][0])-margin
            x2 = int(item['points'][1][0])+margin
            y1 = int(item['points'][0][1])-margin
            y2 = int(item['points'][1][1])+margin
            l = [x1,x2,y1,y2]
            L.append(l)
    if len(L) == 1:
        x_1 = L[0][0]
        x_2 = L[0][1]
        y_1 = L[0][2]
        y_2 = L[0][3]
    else:
        for i in range(len(L)):
            K.append(L[i][0])
        ind = K.index(max(K))
        x_1 = L[ind][0]
        x_2 = L[ind][1]
        y_1 = L[ind][2]
        y_2 = L[ind][3]
    # 保持比例处理逻辑,使长宽比例尽可能贴近1.0
    if (y_2-y_1)/(x_2-x_1) > 1.5:# or :
        paddling = ((y_2-y_1)-(x_2-x_1))//2
        x_2+=paddling
        x_1-=paddling
    elif (y_2-y_1)/(x_2-x_1) <0.6:
        paddling = ((x_2-x_1)-(y_2-y_1))//2
        y_2+=paddling
        y_1-=paddling

    # small_img = img[int(y_1):int(y_2),int(x_1):int(x_2)]
    small_img = img.crop((x_1,y_1,x_2,y_2))
    w,h = small_img.size[0],small_img.size[1]
    # small_img = Image.fromarray(cv2.cvtColor(small_img, cv2.COLOR_BGR2RGB))
    # 图像增强
    enh_con = ImageEnhance.Contrast(small_img)
    contrast = 15
    small_img = enh_con.enhance(contrast)
    small_img = ImageEnhance.Sharpness(small_img).enhance(3)

    # w,h = small_img.size[0],small_img.size[1]
    small_img = small_img.resize((256,256),Image.ANTIALIAS)
    norm_x = x_1
    norm_y = y_1
    return small_img,norm_x,norm_y,w,h

def get_cord(data_update, norm_x, norm_y, w, h, label):
    """
    获取label对应点的坐标
    """
    shapes_update = data_update['shapes']

    for item in shapes_update:
        if item['label'] == label:
            x = item['points'][0][0] - norm_x
            y = item['points'][0][1] - norm_y
            x = int(x * (256/w))
            y = int(y * (256/h))
            cord = [x,y]
            # print(cord)
            return cord

def get_small_json(image,cords,file):
    """
    生成小图对应的json
    :param image：图片
    :param cords: 坐标列表[ns,ne,ne1,ne2,ne3],顺序不能错
    """
    shapes = []
    for i in range(len(cords)):
        shapes.append({"label":str(i), "points":[cords[i]],"shape_type":"point","flags":{}})
    # imagedata = utils.img_arr_to_b64(image).decode('utf-8')
    result = {
        "imageHeight": 256,
        "imageWidth": 256,
        "shapes": shapes,
        "imagePath": file,
        "version": "4.5.7",
        "imageData": None
    }
    return result



def save_img_and_json(files,img_save_path,label_save_path,img_path):
    """
    生成小图和对应的json
    """
    path = '/data1/shanglin/data_process/direction/data/n1-1130-1150-json'
    n1_files = os.listdir(path)
    num = 0
    for file in tqdm(files):
        if file.split(".")[0]+'.json' in n1_files:
            with open(os.path.join(path,file.split('.')[0]+'.json'),'rb') as f:
                n1data = json.load(f)
            if not n1_exists(n1data):
                # 若没有n1标签，则跳过
                continue
            else:
                num += 1
                img = Image.open(os.path.join(img_path,file))
                # img = cv2.imdecode(np.fromfile(os.path.join(img_path, file), dtype=np.uint8), -1)
                # 生成被切分后的小图
                
                cut_img, norm_x, norm_y, w, h = get_small_img(img, n1data)
                # print(n1data["imagePath"])
                # print(norm_x,norm_y,w,h)
                cords = []
                labels = ["Ns","Ne","Ne-1","Ne-2","Ne-3"]
                for lb in labels:
                    # 求出每个小图中label对应的坐标，形成列表
                    cords.append(get_cord(n1data,norm_x,norm_y,w,h,lb))
                
                if cords[0] is not None and cords[1] is not None:
                    # cv2.imencode(os.path.join(img_save_path, '{}.png'.format(num)), cut_img)[1].tofile(os.path.join(img_save_path, file))
                    cut_img.save(os.path.join(img_save_path,file))
                    with open(os.path.join(label_save_path, file.split('.')[0] + '.json'), 'w', encoding='utf-8') as new_f:
                        json.dump(get_small_json(cut_img, cords, file), new_f, ensure_ascii=False)

if __name__ == '__main__':
    # data_dir为生成训练和测试集的路径
    train_dir = "/data1/shanglin/data_process/direction/data/data1201/train"
    val_dir = "/data1/shanglin/data_process/direction/data/data1201/val"
    train_img_path = train_dir+'/img'
    train_label_path = train_dir+'/label'
    val_img_path = val_dir+'/img'
    val_label_path = val_dir+'/label'
    PATHS = [train_img_path, train_label_path, val_img_path, val_label_path]
    for path in PATHS:
        if not os.path.exists(path):
            os.mkdir(path)
        else:
            shutil.rmtree(path)
            os.mkdir(path)
    # 随机数固定
    random.seed(9)

    img_path = '/data1/shanglin/data_process/direction/data/data-1150-png'
    files = os.listdir(img_path)
    val_fraction = 0.1
    files_val = random.sample(files,int(len(files)*0.1))
    for i in files_val:
        files.remove(i)
    print('正在生成训练集')
    save_img_and_json(files,train_img_path,train_label_path,img_path)
    print('正在生成验证集')
    save_img_and_json(files_val,val_img_path,val_label_path,img_path)










