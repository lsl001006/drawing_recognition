from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from .sample.ddd import DddDataset
from .sample.exdet import EXDetDataset
from .sample.ctdet import CTDetDataset
from .sample.multi_pose import MultiPoseDataset

from .dataset.coco import COCO
from .dataset.pascal import PascalVOC
from .dataset.kitti import KITTI
from .dataset.coco_hp import COCOHP
# from .dataset.drawing import Drawing
# from .dataset.draws import DRAWS
from .dataset.draw2 import DRAW2
from .dataset.draw3 import DRAW3
from .dataset.draw4 import DRAW4


dataset_factory = {
  'coco': COCO,
  'pascal': PascalVOC,
  'kitti': KITTI,
  'coco_hp': COCOHP,
  # 'draw2':DRAW2,
  'draw3':DRAW3,
  'draw4':DRAW4
}

_sample_factory = {
  'exdet': EXDetDataset,
  'ctdet': CTDetDataset,
  'ddd': DddDataset,
  'multi_pose': MultiPoseDataset
}


def get_dataset(dataset, task):
  # 这个类继承了两个父类，融合了数据集类和采样类
  class Dataset(dataset_factory[dataset], _sample_factory[task]):
    pass
  return Dataset
  
