from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2
import numpy as np
from progress.bar import Bar
import time
import torch
import os

try:
    from external.nms import soft_nms
except:
    print('NMS not imported! If you need it,'
          ' do \n cd $CenterNet_ROOT/src/lib/external \n make')
from models.decode import ctdet_decode
from models.utils import flip_tensor
from utils.image import get_affine_transform
from utils.post_process import ctdet_post_process
from utils.debugger import Debugger

from .base_detector import BaseDetector


class CtdetDetector(BaseDetector):
    def __init__(self, opt):
        super(CtdetDetector, self).__init__(opt)
        self.debugImgCnt = 1
        if self.opt.debug >= 1:
            # 若Debug的输出路径不存在，则创建该路径
            self.outputPath = os.path.abspath(self.opt.debugOut)
            if not os.path.isdir(self.outputPath):
                os.mkdir(self.outputPath)

    # 中间 主处理过程，完成模型的正向推断

    def process(self, images, return_time=False):
        with torch.no_grad():
            output = self.model(images)[-1]
            hm = output['hm'].sigmoid_()
            wh = output['wh']
            reg = output['reg'] if self.opt.reg_offset else None
            if self.opt.flip_test:
                hm = (hm[0:1] + flip_tensor(hm[1:2])) / 2
                wh = (wh[0:1] + flip_tensor(wh[1:2])) / 2
                reg = reg[0:1] if reg is not None else None
            # 多GPU之间多同步？
            torch.cuda.synchronize()
            # 记录下推断结束时间戳（并返回）
            forward_time = time.time()
            # 输出结果解码（这个解码过程赋予了输出张量中每个元素的语义）
            # dets的shape为 batchsize * K * 6 。其中6为：bbox4个值（左上点+右下点） + 得分 + 类别
            dets = ctdet_decode(
                hm, wh, reg=reg, cat_spec_wh=self.opt.cat_spec_wh, K=self.opt.K)

        if return_time:
            return output, dets, forward_time
        else:
            return output, dets

    # 后处理，具体包含？？？TODO 待确认
    def post_process(self, dets, meta, scale=1):
        dets = dets.detach().cpu().numpy()
        dets = dets.reshape(1, -1, dets.shape[2])
        dets = ctdet_post_process(
            dets.copy(), [meta['c']], [meta['s']],
            meta['out_height'], meta['out_width'], self.opt.num_classes)
        for j in range(1, self.num_classes + 1):
            dets[0][j] = np.array(dets[0][j], dtype=np.float32).reshape(-1, 5)
            dets[0][j][:, :4] /= scale
        return dets[0]

    # 融合不同尺度下的检测结果
    def merge_outputs(self, detections):
        results = {}
        for j in range(1, self.num_classes + 1):
            results[j] = np.concatenate(
                [detection[j] for detection in detections], axis=0).astype(np.float32)
            if len(self.scales) > 1 or self.opt.nms:
                soft_nms(results[j], Nt=0.5, method=2)
        scores = np.hstack(
            [results[j][:, 4] for j in range(1, self.num_classes + 1)])
        if len(scores) > self.max_per_image:
            kth = len(scores) - self.max_per_image
            thresh = np.partition(scores, kth)[kth]
            for j in range(1, self.num_classes + 1):
                keep_inds = (results[j][:, 4] >= thresh)
                results[j] = results[j][keep_inds]
        return results

    def debug(self, debugger, images, dets, output, scale=1):
        pred_img_name = str(self.debugImgCnt) + \
            '-out_pred_{:.1f}'.format(scale)
        pred_hm_img_name = str(self.debugImgCnt) + \
            '-pred_hm_{:.1f}'.format(scale)
        detection = dets.detach().cpu().numpy().copy()
        detection[:, :, :4] *= self.opt.down_ratio
        for i in range(1):
            img = images[i].detach().cpu().numpy().transpose(1, 2, 0)
            img = ((img * self.std + self.mean) * 255).astype(np.uint8)
            # 生成热力图
            pred = debugger.gen_colormap(
                output['hm'][i].detach().cpu().numpy())
            # 保存热力图
            debugger.add_blend_img(img, pred, img_id=pred_hm_img_name)
            debugger.add_img(img, img_id=pred_img_name)
            for k in range(len(dets[i])):
                if detection[i, k, 4] > self.opt.center_thresh:
                    debugger.add_coco_bbox(detection[i, k, :4], detection[i, k, -1],
                                           detection[i, k, 4],
                                           img_id=pred_img_name)

    def show_results(self, debugger, image, results):
        img_id = str(self.debugImgCnt) + "-"
        debugger.add_img(image, img_id=img_id)
        for j in range(1, self.num_classes + 1):
            for bbox in results[j]:
                # 大于门限的bbox才会显示出来
                if bbox[4] > self.opt.vis_thresh:
                    debugger.add_coco_bbox(
                        bbox[:4], j - 1, bbox[4], img_id=img_id)
        # show_all_imgs函数内调用到是cv2.imshow，采用qt做显示。可通过x11 forwarding映射到本地来实现弹窗显示。
        # debugger.show_all_imgs(pause=self.pause)
        # 改为保存图片
        debugger.save_all_imgs(self.outputPath)
        self.debugImgCnt += 1
