import os
import json
import re
import pdb
from PIL import Image
# from modifyres import genLbdict
from rich.progress import track

info_template = {
            "info": {
                "像素面积": {
                    "全部面积": 0,
                    "设备面积": 0,
                    "剩余面积": 0
                },
                "图纸信息": {
                    "类型": "机房平面/铁塔/楼面塔",
                    "名称": "",
                    "图号": "",
                    "负责人": "",
                    "绘图人": "",
                    "审核人": "",
                    "设计院": "",
                    "图框": "A3/A4",
                    "文件名": ""
                }
            },       
}

obj_list = {
        "obj": []
}

obj_each = {
            "id": "",
            "type": "box/circle/polygon/polyline/model/text/table",
            "shape": "rect/circle/polygon/polyline/point/att1-1",
            "coord": [
                [0, 0],
                [1, 1]
            ],
            "rotate": 0,
            "semantic": {
                "element": "",
                "links": [
                    # {"id":""},
                    # {"id":""}
                ]
            },
		    "ext": {}
}

class_name = [
      'a1-1', 'a1-2', 'a1-3', 'a1-4', 'a3', 'a4', 'an', 'b1', 'c1', 'd1', 'g1', 'h1', 'l1', 'n1', 'p1-1',
      'p1-2', 's1', 't1', 't10', 't3-l', 't3-m', 't3-r', 't7', 't9', 'tn', 'v1','va1', 'va3', 'va4', 'van',
      'vb1', 'vp1-1', 'vp1-2', 'vp1-3', 'vp1-4', 'vp1-5', 'vp1-6', 'vp2-1', 'vp2-2', 'vp2-3', 'vpn', 'vs1',
      'vt1', 'vt10-1', 'vt10-2', 'vt10-n', 'vt3', 'vt4', 'vt7-1', 'vt7-2', 'vt9', 'vtn', 'z1', 'x1', 'k1',
      'x2', 'btl', 'tl', 'sm', 'wz1', 'wz1-l', 'wz2', 'wz3', 'wz3-l', 'wz4','j1','xtl','wztl','wlb'
]

def genLbdict(lbpath):
    """
    生成标签-序号对应的字典
    :param lbpath: 标签文件labels.txt路径
    """
    lbdict = {}
    with open(lbpath,'r',encoding='utf-8') as f:
        lblist = f.readlines()
    for i,each in enumerate(lblist):
        tmp = each.split(' ')
        tmp2 = re.sub('\n','',tmp[1])
        lbdict[tmp2] = i+1
    return lbdict

img_path = "/data1/shanglin/data_process/rebuild/input_pics"
img_list = os.listdir(img_path)
label_path = "/data1/shanglin/data_process/rebuild/labels.txt"
data_path = "/data1/shanglin/data_process/rebuild/modified_results/results_stage5.json"
gen_path = "results_final"
if not os.path.exists(gen_path):
    os.mkdir(gen_path)
# 加载标签-序号对应字典
lbdict = genLbdict(label_path)
text_labels = ['wz1','wz3','v1','d1','wztl','h1','sm']
text_cats = [lbdict[p] for p in text_labels]
table_labels = ["wlb",'btl']
table_cats = [lbdict[p] for p in table_labels]
non_device_label = ['wz1','wz3','sm','v1','d1','wztl','h1','a1-1','a1-2','a1-3','a1-4','g1','p1-1','p1-2','btl','wlb']
non_device_cats = [lbdict[p] for p in non_device_label]




def calc_Area(data, i, img_path):
    """
    计算面积，全部面积，设备面积，剩余面积
    """
    filename = data[i][0]["file_name"]+'.png'
    img = Image.open(os.path.join(img_path, filename))
    w, h = img.size[0], img.size[1]
    totalArea = w*h
    deviceArea = 0
    for j in range(len(data[i])):
        if data[i][j]["score"] > 0.3  and data[i][j]['category_id'] not in non_device_cats:
            wd = data[i][j]["bbox"][2]
            hd = data[i][j]["bbox"][3]
            deviceArea += wd*hd
    restArea = totalArea - deviceArea
    return totalArea, deviceArea, restArea


def get_obj_info(data, i, res2):
    """
    将识别出的物件信息填入res2中
    """
    global obj_list
    ori = res2
    for j in range(len(data[i])):
        res2 = ori
        if data[i][j]["score"] > 0.3 and data[i][j]["file_name"]+'.png' in img_list:

            res2["id"] = data[i][j]["mask_id"]
            
            if data[i][j]["category_id"] not in non_device_cats:
                res2["type"] = "model"
            elif data[i][j]["category_id"] in text_cats:
                res2["type"] = "text"
                if data[i][j]["category_id"] in ["wz1","wz3"] and "match_list" in data[i][j].keys():
                    res2["semantic"]["links"] = [each for each in data[i][j]["match_list"]]
                if 'WzInfo' in data[i][j].keys():
                    res2["ext"]["文字识别结果"] = data[i][j]["WzInfo"]

            elif data[i][j]["category_id"] in table_cats:
                res2["type"] = "table"
            
            res2["shape"] = "rect"
            xl,yl = data[i][j]["bbox"][0], data[i][j]["bbox"][1]
            w,h = data[i][j]["bbox"][2], data[i][j]["bbox"][3]
            xr, yr = xl+w, yl+h
            res2["coord"] = [[xl,yl],[xr,yr]]
            res2["semantic"]["element"] = class_name[data[i][j]["category_id"]-1]

            if class_name[data[i][j]["category_id"]-1] == "n1" and "n1_cords" in data[i][j].keys():
                res2["ext"]["建北磁北坐标"] = [each for each in data[i][j]["n1_cords"]]
        obj_list['obj'].append(res2)
    return obj_list

def gen_paper_info(data, i, res1):
    """
    生成图纸信息
    """
    res1["info"]["图纸信息"]["类型"] = "铁塔"
    res1["info"]["图纸信息"]["名称"] = data[i][0]["file_name"]
    res1["info"]["图纸信息"]["图号"] = ""
    res1["info"]["图纸信息"]["负责人"] = ""
    res1["info"]["图纸信息"]["绘图人"] = ""
    res1["info"]["图纸信息"]["审核人"] = ""
    res1["info"]["图纸信息"]["设计院"] = ""
    res1["info"]["图纸信息"]["图框"] = "A3/A4"
    res1["info"]["图纸信息"]["文件名"] = ""
    return res1



if __name__ == "__main__":
    
    if not os.path.exists(gen_path):
        os.mkdir(gen_path)
    with open(data_path, 'rb') as f:
        data = json.load(f)
    
    for i in track(range(len(data))):
        # 对于data中的每一个图
        # 计算info中的面积参数
        res1 = info_template
        res2 = obj_list
        tA, dA, rA = calc_Area(data, i, img_path)
        res1["info"]["像素面积"]["全部面积"] = tA
        res1["info"]["像素面积"]["设备面积"] = dA
        res1["info"]["像素面积"]["剩余面积"] = rA
        # print(res1)

        # gen_paper_info 调用现有函数去获取图纸信息 FIXME
        res1 = gen_paper_info(data, i, res1)

        obj_each = get_obj_info(data, i, obj_each)

        result = res1
        print('--------------------------------------------------------------------------------')
        print(res1)
        print('')
        print(obj_each)
        
        result["obj"] = obj_each["obj"]
        # print(result)
        print('--------------------------------------------------------------------------------')
        pdb.set_trace()
        with open(os.path.join(gen_path, data[i][0]["file_name"]+'.json'),'w',encoding='utf-8') as f:
            json.dump(result, f, ensure_ascii=False, indent=4) 


    

    