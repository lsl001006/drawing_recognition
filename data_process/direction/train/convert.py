# import cv2
# img = cv2.imread('/data1/guojie/direction/data/val/small_imgs/ZQ1312-诺德明府东北（铁塔公司）5G无线基站天馈线安装示意图.png')
# gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
 
# # retval, dst = cv2.threshold(gray, 110, 255, cv2.THRESH_BINARY)
# # 最大类间方差法(大津算法)，thresh会被忽略，自动计算一个阈值
# retval, dst = cv2.threshold(gray, 254, 255, cv2.THRESH_BINARY)
# dst = cv2.cvtColor(dst,cv2.COLOR_GRAY2BGR)
# cv2.imwrite('/data1/guojie/direction/train/convert.png',dst)

from PIL import Image
from PIL import ImageEnhance
 
img = Image.open('/data1/shanglin/data_process/direction/train/ZQ1312-诺德明府东北（铁塔公司）5G无线基站天馈线安装示意图.png')
# img.show()
print(img.size)
#对比度增强  
enh_con = ImageEnhance.Contrast(img)  
contrast = 15
img_contrasted = enh_con.enhance(contrast) 
img_contrasted.resize(img_contrasted.size, Image.ANTIALIAS) 
img_contrasted.save("/data1/shanglin/data_process/direction/train/ZQ1312-诺德明府东北（铁塔公司）5G无线基站天馈线安装示意图_enhance.png")