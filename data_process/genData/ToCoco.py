import pathlib
import os
import shutil
from labelme2coco import labelme2coco

# from pycocotools.coco import COCO

basePath = pathlib.Path(".")

annoDir = os.path.join(basePath, "annotations")
if os.path.exists(annoDir):
    shutil.rmtree(annoDir)
os.mkdir(annoDir)
labelFilePath = os.path.abspath("labels.txt")
trainSetDir = "/home/zhaobo/my/rp_train_merge"
valSetDir = "/home/zhaobo/my/rp_val_merge"

print("正在将训练集转为coco格式")
labelme2coco(labelFilePath, trainSetDir, os.path.join(annoDir, "train.json"))
print("正在将验证集转为coco格式")
labelme2coco(labelFilePath, valSetDir, os.path.join(annoDir, "val.json"))