# -*- coding: utf-8 -*-
import numpy as np
import scipy.io as sio
import glob
import os
import torch
import torch.utils.data
import torchvision.transforms.functional
import cv2
import json
from Transformers import Mouse_resized
import pdb

def read_dataset(path):
	"""
	Read training dataset or validation dataset.
	:param path: The path of dataset.
	:return: The list of filenames.
	"""
	image_list = glob.glob(os.path.join(path, '*.png'))
	
	return image_list



def read_json(json_path):
	"""
	Read joints.json file.
	:param mode: 'lspet' or 'lsp'
	:param path: The path of joints.mat.
	:param image_list: The array of image filenames.
	:return:
	"""
	key_point_list = []
	name_list=[]
	images = os.listdir(json_path)
	
	for i in range(len(images)):
		name = os.path.splitext(images[i])
		name_list.append(name[0])
		path_label = os.path.join(json_path,images[i])
		if os.path.isfile(path_label):
			data_list = [[] for i in range(2)]#################################
			data = json.load(open(path_label, 'rb'))
			for j in range(len(data["shapes"])):
				label = data["shapes"][j]["label"]
				data_list[int(label) - 1].append(data["shapes"][j]['points'][0])

			key_point_list.append(data_list)
	
	return key_point_list,name_list

def read_sidemap_json(sidemap_path):
	sidemap_list = []
	sidemap_name = []

	sidemaps = os.listdir(sidemap_path)
	for i in range(len(sidemaps)):
		name = os.path.splitext(sidemaps[i])
		sidemap_name.append(name[0])

		path_label = sidemap_path + sidemaps[i]
		if os.path.isfile(path_label):
			data1 = json.load(open(path_label, 'rb'))
			data=[]
			data.append(data1)
			sidemap_list.append(np.array(data))
	return 	sidemap_list,sidemap_name

def gaussian_kernel(size_w, size_h, center_x, center_y, sigma):
	grid_y, grid_x = np.mgrid[0:size_h, 0:size_w]
	D2 = (grid_x - center_x) ** 2 + (grid_y - center_y) ** 2

	return np.exp(-D2 / 2.0 / sigma / sigma)


class LSP_DATA(torch.utils.data.Dataset):
	def __init__(self, img_path, json_path,stride, transformer=None):
		self.image_list = read_dataset(img_path)
		self.key_point_list,self.name_list=read_json(json_path)

		# self.sidemap_list,self.sidemap_name=read_sidemap_json(sidemap_path)

		self.stride = stride
		self.transformer = transformer
		self.sigma = 1.0
	def __getitem__(self, item):
		image_path = self.image_list[item]
		image = np.array(cv2.imread(image_path), dtype=np.float32)
		
		name=self.name_list[item]

		key_points = self.key_point_list[item]

		# sidemap_list=self.sidemap_list[item]

		scale=[1,1]


		# Expand dataset
		image, key_points = self.transformer(image, key_points, scale)
		h, w, _ = image.shape

		# Generate heatmap
		# stride 就是缩放比例
		size_h = int(h / self.stride)
		size_w = int(w / self.stride)

		heatmap = np.zeros((size_h,size_w,len(key_points)), dtype=np.float32) #

		# Generate the heatmap of all key points
		for i in range(len(key_points)):
			# Resize image from 256 to 64
			if key_points[i]!=[]:
				for j in range(len(key_points[i])):
					x = int(key_points[i][j][0]) * 1.0 / self.stride
					y = int(key_points[i][j][1]) * 1.0 / self.stride
					kernel = gaussian_kernel(size_h=size_h, size_w=size_w, center_x=x, center_y=y, sigma=self.sigma)
					kernel[kernel > 1] = 1
					kernel[kernel < 0.01] = 0
					heatmap[:, :,i] = heatmap[:, :,i]+kernel

		# Generate the heatmap of background
		# heatmap[:, :, 0] = 1.0 - np.max(heatmap[:, :, 1:], axis=2)

		image -= image.mean()

		image = torchvision.transforms.functional.to_tensor(image)
		heatmap = torch.from_numpy(np.transpose(heatmap, (2, 0, 1)))

		# sidemap_list = torch.from_numpy(sidemap_list)
		
		return image.float(), heatmap.float()

	def __len__(self):
		return len(self.image_list)

if __name__ == '__main__':

	img_path = '/data1/guojie/direction/data/small_imgs'
	label_path = '/data1/guojie/direction/data/small_imgs_label'
	data_train = LSP_DATA(img_path,label_path,4,Mouse_resized())
	for i in range(100):
		data = data_train[i]