import numpy as np
import cv2
import torch
import torchvision.transforms.functional as F
import os
import json
import time
from labelme import utils
from gen_data import gaussian_kernel

torch.backends.cudnn.enabled = True
torch.backends.cudnn.benchmark = True


def get_key_points(heatmap, height, width):
    # Get final heatmap
    heatmap = np.asarray(heatmap.cpu().data)
    key_points = [[] for i in range(3)]
    # Get k key points from heatmap6
    for i in range(len(heatmap)):
        x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
        a = [x, y]
        b = heatmap[i]
        key = []
        # Get the coordinate of key point in the heatmap (128,128)
        while (a != [] and heatmap[i][a[0]][a[1]] > 0.5):
            key.append(a)
            for j in range(-3, 3):
                for z in range(-3, 3):
                    if a[0] + j < 128 and a[1] + z < 128:
                        heatmap[i][a[0] + j][a[1] + z] = 0
            x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
            a = [x, y]
            # Calculate the scale to fit original image
        scale_x = width / heatmap[i].shape[1]
        scale_y = height / heatmap[i].shape[0]
        for z in key:
            y = int(z[0] * scale_x)
            x = int(z[1] * scale_y)
            key_points[i].append([x, y])
    return key_points


def draw_image(image, key_points):
    # draw key points
    for i in range(len(key_points)):
        if key_points[i] != []:
            for j in range(len(key_points[i])):
                x = int(key_points[i][j][0])
                y = int(key_points[i][j][1])
                color = (0, 0, 255)
                size = 2
                color1 = (0, 255, 0)
                color2 = (255, 0, 0)
                if i == 0:
                    cv2.line(image, (x, y), (x, y + 5), color1, 3)
                    cv2.line(image, (x, y), (x + 5, y), color1, 3)

                elif i == 1:
                    cv2.line(image, (x, y), (x, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x - 5, y), color1, 3)

                elif i == 2:
                    cv2.line(image, (x, y), (x, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x - 5, y), color1, 3)
                    cv2.line(image, (x, y), (x + 5, y), color1, 3)
                    cv2.line(image, (x, y), (x, y + 5), color1, 3)
    return image


def key_point_true(img_path, path_json):
    data_list = [[] for i in range(3)]
    data = json.load(open(path_json, 'rb'))
    for j in range(len(data["shapes"])):
        label = data["shapes"][j]["label"]
        if int(label) - 1 == 6 or int(label) - 1 == 10 or int(label) - 1 == 12:
            data_list[0].append(data["shapes"][j]['points'][0])
        if int(label) - 1 == 9 or int(label) - 1 == 11 or int(label) - 1 == 13:
            data_list[1].append(data["shapes"][j]['points'][0])
        if int(label) - 1 == 14:
            data_list[2].append(data["shapes"][j]['points'][0])
    return data_list


def pck(key_points, json_path, img_path, flag):
    """
    Use PCK with threshold of .5 of normalized distance (presumably head size)
    """
    key_point_list_true = key_point_true(img_path, json_path)
    scale = [1, 1]
    sum = 0
    for i in range(len(key_points)):
        if key_point_list_true[i] != []:
            for j in range(len(key_point_list_true[i])):
                if len(key_point_list_true[i]) != len(key_points[i]):
                    max_len = max(len(key_point_list_true[i]), len(key_points[i]))
                    if len(key_point_list_true[i]) < max_len:
                        for z in range(0, max_len - len(key_point_list_true)):
                            key_point_list_true[i].append([0, 0])
                    else:
                        for z in range(0, max_len - len(key_points[i])):
                            key_points[i].append([0, 0])
                key_point_list_true[i][j][0] = int(key_point_list_true[i][j][0] * scale[0])
                key_point_list_true[i][j][1] = int(key_point_list_true[i][j][1] * scale[1])
                error = np.linalg.norm(np.array(key_point_list_true[i][j], int) - key_points[i][j])
                sum = sum + error
    if flag == 1:
        print("平均误差为" + str(sum / len(key_point_list_true)))
    else:
        return sum / len(key_point_list_true)


def test_erro(model_save_path):
    data_path = r'./data/'
    json_path1 = data_path + r'patch_label/test/'
    img_path = data_path + r'patch_img/test/'

    image_path = os.listdir(img_path)
    model = torch.load(model_save_path).cuda()
    test_erro_img = 0
    for i in range(len(image_path)):
        name = os.path.splitext(image_path[i])
        image = cv2.imread(img_path + image_path[i])
        json_path = json_path1 + str(name[0]) + '.json'
        height, width, _ = image.shape
        image = np.asarray(image, dtype=np.float32)
        image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_CUBIC)
        image -= image.mean()
        image = F.to_tensor(image)
        image = torch.unsqueeze(image, 0).cuda()
        model.eval()
        input_var = torch.autograd.Variable(image)
        with torch.no_grad():
            result = model(input_var)
            pre_img = result[0, 3, :, :, :]
            key_points = get_key_points(pre_img, height=height, width=width)
            test_erro_img = pck(key_points, json_path, img_path + image_path[i], flag=0) + test_erro_img
    test_erro = test_erro_img / len(image_path)
    return test_erro


def make_json(img_path, point_list, jsonsave_path):
    '''
    Forecast point set JSON file saved
    '''
    image = cv2.imread(img_path, 1)
    labelme_data = {}
    labelme_data["version"] = "4.5.7"
    labelme_data["flags"] = {}
    labelme_data["shapes"] = []
    for i in range(len(point_list)):
        for j in range(len(point_list[i])):
            labelme_dict = {}
            labelme_dict["label"] = str(i + 1)
            labelme_dict["points"] = [point_list[i][j]]
            labelme_dict["group_id"] = None
            labelme_dict["shape_type"] = "point"
            labelme_dict["flags"] = {}
            labelme_data["shapes"].append(labelme_dict)
    labelme_data["imagePath"] = img_path
    labelme_data["imageData"] = utils.img_arr_to_b64(image).decode('utf-8')
    labelme_data["imageHeight"] = image.shape[0]
    labelme_data["imageWidth"] = image.shape[1]
    labelme_data1 = {"version": "4.5.7", "flags": {}, "shapes": labelme_data["shapes"],
                     "imagePath": labelme_data["imagePath"],
                     "imageData": labelme_data["imageData"],
                     "imageHeight": labelme_data["imageHeight"],
                     "imageWidth": labelme_data["imageWidth"]
                     }
    with open(jsonsave_path, 'w') as f:
        json.dump(labelme_data1, f, indent=2)
        print("加载入文件完成...")


def model_pre(data_path):
    '''
    预测数据
    预测点数据保存到json文件，显示到图片上
    :param data_path: 数据所在路径
    :return 无
    '''
    # pred_path   预测点显示到图片上的文件路径
    # json_path1  标注的json文件路径
    # img_path    预测图片所在路径
    # jsonsave_path  预测点保存到json文件的路径
    pred_path = data_path + r'train_test/pred/'
    json_path1 = data_path + r'train_test/label_test_9/'
    img_path = data_path + r'train_test/img_test_9/'

    jsonsave_path = data_path + r'pred_small_json/'

    image_path = os.listdir(img_path)
    model = torch.load('./models/hourglass_1018.pth').cuda()  # 模型加载
    for i in range(len(image_path)):
        name = os.path.splitext(image_path[i])
        print("图片    " + name[0] + "*****************************")
        image = cv2.imread(img_path + image_path[i])
        json_path = json_path1 + str(name[0]) + '.json'
        height, width, _ = image.shape
        image = np.asarray(image, dtype=np.float32)
        image = cv2.resize(image, (256, 256), interpolation=cv2.INTER_CUBIC)
        image -= image.mean()
        image = F.to_tensor(image)
        image = torch.unsqueeze(image, 0).cuda()
        model.eval()
        input_var = torch.autograd.Variable(image)
        with torch.no_grad():
            result = model(input_var)
            pre_img = result[0, 3, :, :, :]
            key_points = get_key_points(pre_img, height=height, width=width)
            # pck(key_points, json_path, img_path + image_path[i], flag=1)
            make_json(img_path+image_path[i],key_points,jsonsave_path+name[0]+'.json')
            image = draw_image(cv2.imread(img_path + image_path[i]), key_points)
            # cv2.imshow('test image', image)
            # cv2.waitKey(0)
            # cv2.imwrite(pred_path + name[0] + '_ans.png', image)


if __name__ == "__main__":
    model_pre(data_path=r'../data/')
