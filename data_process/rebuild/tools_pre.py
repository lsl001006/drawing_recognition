import numpy as np
import os
import cv2
import json
import glob
import torch
import torchvision.transforms.functional as F
# from paddleocr import PaddleOCR, draw_ocr
import math
import copy



def find_min_loss(A, B, flag):
    value = []
    value1 = []
    min=0
    min_flag=0
    if(flag==1):
        for point1 in A:
            value.append(point1)
            C=B[:]
            if point1 in C:
                C.remove(point1)
            for point2 in C:
                value.append(point2)
                if (value[0][1]>value[1][1]-5)and(value[0][1]<value[1][1]+5)and(value[0][0]<value[1][0]):
                    if min_flag==0:
                        min_err = abs(value[0][1] - value[1][1])
                        min=value[1][0]-value[0][0]
                        min_flag=1
                        value1.append(value[:])
                    if min>value[1][0]-value[0][0]:
                        min=value[1][0]-value[0][0]
                        del(value1[-1])
                        value1.append(value[:])
                    if min_err > abs(value[0][1] - value[1][1])and(min>=value[1][0]-value[0][0]):
                        min_err = abs(value[0][1] - value[1][1])
                        del (value1[-1])
                        value1.append(value[:])
                del(value[-1])
            del(value[-1])
            min_flag = 0
        return(value1)
    if(flag==2):
        for point1 in A:
            value.append(point1)
            for point2 in B:
                value.append(point2)
                if (value[0][0]>value[1][0]-5)and(value[0][0]<value[1][0]+5)and(value[0][1]<value[1][1]):
                    if min_flag==0:
                        min_err = abs(value[1][0] - value[0][0])
                        min=value[1][1]-value[0][1]
                        min_flag=1
                        value1.append(value[:])
                    if min>value[1][1]-value[0][1]:
                        min=value[1][1]-value[0][1]
                        del(value1[-1])
                        value1.append(value[:])
                    if min_err>abs(value[1][0] - value[0][0])and(min>=value[1][1]-value[0][1]):
                        min_err = abs(value[0][1] - value[1][1])
                        del (value1[-1])
                        value1.append(value[:])
                del(value[-1])
            del(value[-1])
            min_flag = 0
        return(value1)
    if (flag == 3):
        for point1 in A:
            value.append(point1)
            for point2 in B:
                value.append(point2)
                if (value[0][1]>value[1][1]-5) and (value[0][1] < value[1][1] + 5) and (value[0][0]>value[1][0]):
                    if min_flag == 0:
                        min_err = abs(value[0][1]-value[1][1])
                        min = value[0][0] - value[1][0]
                        min_flag = 1
                        value1.append(value[:])
                    if min > value[0][0] - value[1][0]:
                        min = value[0][0] - value[1][0]
                        del (value1[-1])
                        value1.append(value[:])
                    if min_err>abs(value[0][1]-value[1][1])and(min>=value[0][0]-value[1][0]):
                        min_err = abs(value[0][1] - value[1][1])
                        del (value1[-1])
                        value1.append(value[:])
                del (value[-1])
            del (value[-1])
            min_flag = 0
        return(value1)
    if(flag==4):
        for point1 in A:
            value.append(point1)
            for point2 in B:
                value.append(point2)
                if (value[0][0]>value[1][0]-5)and(value[0][0]<value[1][0]+5)and(value[0][1]>value[1][1]):
                    if min_flag==0:
                        min_err = abs(value[0][0] - value[1][0])
                        min=value[0][1]-value[1][1]
                        min_flag=1
                        value1.append(value[:])
                    if min>value[0][1]-value[1][1]:
                        min=value[0][1]-value[1][1]
                        del(value1[-1])
                        value1.append(value[:])
                    if (min_err>abs(value[0][0]-value[1][0]))and(min>=value[0][1]-value[1][1]):
                        min_err = abs(value[0][1] - value[1][1])
                        del (value1[-1])
                        value1.append(value[:])
                del(value[-1])
            del(value[-1])
            min_flag = 0
        return(value1)

def read_region_json(json_path):
    data = json.load(open(json_path, 'rb'))
    data_list = []
    for j in range(len(data["shapes"])):
        if data["shapes"][j]['label'] == 'main':
            data_list = data["shapes"][j]['points']
    return data_list

def read_table_json(json_path):
    data = json.load(open(json_path, 'rb'))
    data_list = []
    for j in range(len(data["shapes"])):
        if data["shapes"][j]['label'] == 'table':
            data_list = data["shapes"][j]['points']
    return data_list

def read_json(json_path, point):
    label_encode = {
        'one': 1, 'two': 2, 'three': 3, 'four': 4, 'five': 5, 'six': 6,
        'seven': 7, 'eight': 8, 'nine': 9, 'ten': 10, 'eleven': 11, 'twelve': 12,
        'thirteen': 13, 'fourteen': 14, 'fifteen': 15, 'sixteen': 16, 'seventeen': 17, 'eighteen': 18,
        'nineteen': 19, 'twenty': 20, 'twentyone': 21, 'twentytwo': 22, 'twentythere': 23
    }
    data_list = [[] for i in range(23)]
    data = json.load(open(json_path, 'rb'))
    for j in range(len(data["objects"])):
        label = label_encode[data["objects"][j]["label"]]
        data_list[int(label) - 1].append([data["objects"][j]['location'][0] - int(point[0]),
                                          data["objects"][j]['location'][1] - int(point[1])])
    return data_list

def draw_point_image(image, key_points):
    # draw key points
    for i in range(len(key_points)):
        if key_points[i] != []:
            for j in range(len(key_points[i])):
                x = int(key_points[i][j][0])
                y = int(key_points[i][j][1])
                color = (0, 0, 255)
                size = 2
                color1 = (0, 255, 0)
                color2 = (255, 0, 0)
                if i == 0:
                    cv2.line(image, (x - 5, y - 5), (x + 5, y + 5), color1, 3)
                    cv2.line(image, (x - 5, y + 5), (x + 5, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x, y - 15), color1, 3)
                elif i == 1:
                    cv2.line(image, (x - 5, y - 5), (x + 5, y + 5), color1, 3)
                    cv2.line(image, (x - 5, y + 5), (x + 5, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x, y + 15), color1, 3)
                elif i == 2:
                    cv2.line(image, (x - 5, y - 5), (x + 5, y + 5), color1, 3)
                    cv2.line(image, (x - 5, y + 5), (x + 5, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x + 15, y), color1, 3)
                elif i == 3:
                    cv2.line(image, (x - 5, y - 5), (x + 5, y + 5), color1, 3)
                    cv2.line(image, (x - 5, y + 5), (x + 5, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x - 15, y), color1, 3)
                elif i == 4:
                    cv2.line(image, (x - 5, y - 5), (x + 5, y + 5), color1, 3)
                    cv2.line(image, (x - 5, y + 5), (x + 5, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x - 15, y), color1, 3)
                    cv2.line(image, (x, y), (x + 15, y), color1, 3)
                elif i == 5:
                    cv2.line(image, (x - 5, y - 5), (x + 5, y + 5), color1, 3)
                    cv2.line(image, (x - 5, y + 5), (x + 5, y - 5), color1, 3)
                    cv2.line(image, (x, y), (x, y - 15), color1, 3)
                    cv2.line(image, (x, y), (x, y + 15), color1, 3)
                elif i == 6:
                    cv2.line(image, (x, y), (x, y + 5), color, size)
                    cv2.line(image, (x, y), (x + 5, y), color, size)
                elif i == 7:
                    cv2.line(image, (x, y), (x, y + 5), color, size)
                    cv2.line(image, (x, y), (x - 5, y), color, size)
                elif i == 8:
                    cv2.line(image, (x, y), (x, y - 5), color, size)
                    cv2.line(image, (x, y), (x + 5, y), color, size)
                elif i == 9:
                    cv2.line(image, (x, y), (x, y - 5), color, size)
                    cv2.line(image, (x, y), (x - 5, y), color, size)
                elif i == 10:
                    cv2.line(image, (x, y), (x, y - 5), color, size)
                    cv2.line(image, (x, y), (x + 5, y), color, size)
                    cv2.line(image, (x, y), (x, y + 5), color, size)
                elif i == 11:
                    cv2.line(image, (x, y), (x, y - 5), color, size)
                    cv2.line(image, (x, y), (x - 5, y), color, size)
                    cv2.line(image, (x, y), (x, y + 5), color, size)
                elif i == 12:
                    cv2.line(image, (x, y), (x, y + 5), color, size)
                    cv2.line(image, (x, y), (x - 5, y), color, size)
                    cv2.line(image, (x, y), (x + 5, y), color, size)
                elif i == 13:
                    cv2.line(image, (x, y), (x, y - 5), color, size)
                    cv2.line(image, (x, y), (x - 5, y), color, size)
                    cv2.line(image, (x, y), (x + 5, y), color, size)
                elif i == 14:
                    cv2.line(image, (x, y), (x, y - 5), color, size)
                    cv2.line(image, (x, y), (x - 5, y), color, size)
                    cv2.line(image, (x, y), (x, y + 5), color, size)
                    cv2.line(image, (x, y), (x + 5, y), color, size)
                # blackpillar
                elif i == 15:
                    cv2.line(image, (x, y), (x, y + 5), color2, size)
                    cv2.line(image, (x, y), (x + 5, y), color2, size)
                elif i == 16:
                    cv2.line(image, (x, y), (x, y + 5), color2, size)
                    cv2.line(image, (x, y), (x - 5, y), color2, size)
                elif i == 17:
                    cv2.line(image, (x, y), (x, y - 5), color2, size)
                    cv2.line(image, (x, y), (x + 5, y), color2, size)
                elif i == 18:
                    cv2.line(image, (x, y), (x, y - 5), color2, size)
                    cv2.line(image, (x, y), (x - 5, y), color2, size)
                # circular_blackpillar
                elif i == 19:
                    cv2.circle(image, (x, y), 5, color2, 2)
                # gata
                elif i == 20:
                    cv2.circle(image, (x, y), 5, color2, 2)
                # thread_hole
                elif i == 21:
                    cv2.circle(image, (x, y), 5, color2, 2)
                    cv2.line(image, (x, y), (x, y + 10), color2, size)
                elif i == 22:
                    cv2.circle(image, (x, y), 5, color, 2)
                    cv2.line(image, (x, y), (x, y - 10), color2, size)
    return image

def get_key_points(heatmap, height, width):
    # Get final heatmap
    heatmap = np.asarray(heatmap.cpu().data)
    key_points = [[] for i in range(23)]
    # Get k key points from heatmap6
    for i in range(len(heatmap)):
        x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
        a = [x, y]
        key = []
        # Get the coordinate of key point in the heatmap (46, 46)
        while (a != [] and heatmap[i][a[0]][a[1]] > 0.4):
            key.append(a)
            for j in range(-2, 2):
                for z in range(-2, 2):
                    if a[0] + j < 64 and a[1] + z < 64:
                        heatmap[i][a[0] + j][a[1] + z] = 0
            x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
            a = [x, y]
            # Calculate the scale to fit original image
        scale_x = width / heatmap[i].shape[1]
        scale_y = height / heatmap[i].shape[0]
        for z in key:
            y = int(z[0] * scale_x)
            x = int(z[1] * scale_y)
            key_points[i].append([x, y])
    return key_points

def model_pre(image, model_size=(512, 512), model_path='./models/hourglass_0513.pth'):
    old_image = image.copy()
    model = torch.load(model_path).cuda()  # 模型加载
    height, width, _ = image.shape
    image = np.asarray(image, dtype=np.float32)
    image = cv2.resize(image, model_size, interpolation=cv2.INTER_CUBIC)
    image -= image.mean()
    image = F.to_tensor(image)
    image = torch.unsqueeze(image, 0).cuda()
    model.eval()
    input_var = torch.autograd.Variable(image)
    with torch.no_grad():
        result, _ = model(input_var)
        pre_img = result[0, 5, :, :, :]
        key_points = get_key_points(pre_img, height=height, width=width)
    return key_points

def img_show(image=[], rectanges=[], black_rectanges=[], line_hole_rectanges=[], tag_line=[], wall_box=[]):
    img = image
    for i in range(len(rectanges)):
        cv2.rectangle(img, (int(rectanges[i][0][0]), int(rectanges[i][0][1])),
                      (int(rectanges[i][1][0]), int(rectanges[i][1][1])), (0, 0, 255), 2)
    for i in range(len(black_rectanges)):
        cv2.rectangle(img, (int(black_rectanges[i][0][0]), int(black_rectanges[i][0][1])),
                      (int(black_rectanges[i][1][0]), int(black_rectanges[i][1][1])), (255, 255, 0), 2)

    for i in range(len(line_hole_rectanges)):
        cv2.rectangle(img, (int(line_hole_rectanges[i][0][0]), int(line_hole_rectanges[i][0][1])),
                      (int(line_hole_rectanges[i][1][0]), int(line_hole_rectanges[i][1][1])), (255, 0, 0), 1)
    for i in range(len(tag_line)):
        cv2.line(img, (int(tag_line[i][0][0]), int(tag_line[i][0][1])),
                 (int(tag_line[i][1][0]), int(tag_line[i][1][1])), (255, 0, 0), 2)
    for i in range(len(wall_box)):
        cv2.rectangle(img, (int(wall_box[i][0][0]), int(wall_box[i][0][1])),
                      (int(wall_box[i][1][0]), int(wall_box[i][1][1])), (64, 64, 64), 2)
    # cv2.namedWindow("img", 1)  # 创建一个窗口
    # cv2.resizeWindow("img", 1024, 1024)
    # cv2.imshow('img', img)  # 显示原始图片
    # cv2.waitKey()
    return img

'''
A是一个点的坐标位置，即出发点
B是一个点的列表，有多个点，是需要探索的点空间
flag=0:A向图片的正上方寻找
flag=1:A向图片中的正右方寻找
flag=2:A向图片中的正下方寻找
flag=3：A向图片中的正左方寻找
flag=4:A向任意方向找，距离最近的点
'''
def find_min(A, B, flag):
    C = []
    if flag == 0:
        for i in range(len(B)):
            if B[i][1] - A[1] < 0 and abs(B[i][0] - A[0]) < 5:
                C.append(B[i])
        if C == []:
            return []
        d = [math.sqrt((i[0] - A[0]) ** 2 + (i[1] - A[1]) ** 2) for i in C]
        return C[d.index(min(d))]
    if flag == 1:
        for i in range(len(B)):
            if B[i][0] - A[0] > 0 and abs(B[i][1] - A[1]) < 5:
                C.append(B[i])
        if C == []:
            return []
        d = [math.sqrt((i[0] - A[0]) ** 2 + (i[1] - A[1]) ** 2) for i in C]
        return C[d.index(min(d))]
    if flag == 2:
        for i in range(len(B)):
            if abs(B[i][0] - A[0]) < 5 and (A[1] - B[i][1]) < 0:
                C.append(B[i])
        if C == []:
            return []
        d = [math.sqrt((i[0] - A[0]) ** 2 + (i[1] - A[1]) ** 2) for i in C]
        return C[d.index(min(d))]
    if flag == 3:
        for i in range(len(B)):
            if (A[0] - B[i][0]) > 0 and abs(B[i][1] - A[1]) < 5:
                C.append(B[i])
        if C == []:
            return []
        d = [math.sqrt((i[0] - A[0]) ** 2 + (i[1] - A[1]) ** 2) for i in C]
        return C[d.index(min(d))]
    if flag == 4:
        for i in range(len(B)):
            C.append(B[i])
        if C == []:
            return []
        d = [math.sqrt((i[0] - A[0]) ** 2 + (i[1] - A[1]) ** 2) for i in C]
        return C[d.index(min(d))]

def find_device(point_list):
    box_list = []
    left_up_list = point_list[6] + point_list[10] + point_list[12] + point_list[14]
    right_up_list = point_list[7] + point_list[11] + point_list[12] + point_list[14]
    right_down_list = point_list[9] + point_list[11] + point_list[13] + point_list[14]
    left_down_list = point_list[8] + point_list[10] + point_list[13] + point_list[14]
    for i in range(len(left_up_list)):
        box = []
        left_up = copy.copy(left_up_list[i])
        right_up = copy.copy(find_min(left_up, right_up_list, 1))
        if right_up != []:
            right_down = copy.copy(find_min(right_up, right_down_list, 2))
            if right_down != []:
                left_down = copy.copy(find_min(right_down, left_down_list, 3))
                if left_down != []:
                    left_up_tem = copy.copy(find_min(left_down, left_up_list, 0))
                    if left_up_tem != []:
                        d2 = math.sqrt((left_up_tem[0] - left_up[0]) ** 2 + (left_up_tem[1] - left_up[1]) ** 2)
                        if d2 < 10:
                            box.append(copy.copy(left_up))
                            box.append(copy.copy(right_down))
                            box_list.append(box)
    return box_list

def fix_rectange(rectanges, w, h):
    rectanges = sorted(rectanges, key=lambda x: x[1])
    res = []
    if rectanges == []:
        return []
    res.append(rectanges[0])
    for i in range(1, len(rectanges)):
        d2 = math.sqrt((rectanges[i][0][0] - rectanges[i][1][0]) ** 2 + (rectanges[i][0][1] - rectanges[i][1][1]) ** 2)
        if d2 < max(w, h) / 2:
            if rectanges[i][1] == res[-1][1]:
                d1 = math.sqrt(
                    (res[-1][0][0] - res[-1][1][0]) ** 2 + (res[-1][0][1] - res[-1][1][1]) ** 2)
                if d2 < d1:
                    res.pop()
                    res.append(rectanges[i])
            else:
                res.append(rectanges[i])
    return res

def find_black_sides(point_list):
    box_list = []
    left_up_list = point_list[15]
    right_up_list = point_list[16]
    right_down_list = point_list[18]
    left_down_list = point_list[17]
    for i in range(len(left_up_list)):
        box = []
        left_up = copy.copy(left_up_list[i])
        right_up = copy.copy(find_min(left_up, right_up_list, 1))
        if right_up != []:
            right_down = copy.copy(find_min(right_up, right_down_list, 2))
            if right_down != []:
                left_down = copy.copy(find_min(right_down, left_down_list, 3))
                if left_down != []:
                    box.append(copy.copy(left_up))
                    box.append(copy.copy(right_down))
                    box_list.append(box)
    return box_list

def find_linehole(point_list):
    box_list = []
    left_up_list = point_list[21]
    right_down_list = point_list[22]
    for i in range(len(left_up_list)):
        box = []
        left_up = copy.copy(left_up_list[i])
        right_down = copy.copy(find_min(left_up, right_down_list, 4))
        if right_down != []:
            box.append(copy.copy(left_up))
            box.append(copy.copy(right_down))
        box_list.append(box)
    return box_list

def find_wall(rectanges, w, h):
    rectanges = sorted(rectanges, key=lambda x: x[1])
    res = []
    if rectanges == []:
        return []
    res.append(rectanges[0])
    for i in range(1, len(rectanges)):
        d2 = math.sqrt((rectanges[i][0][0] - rectanges[i][1][0]) ** 2 + (rectanges[i][0][1] - rectanges[i][1][1]) ** 2)
        if d2 > max(h, w) / 2:
            res.append(rectanges[i])
    return res

def find_tag(point_list):
    '''
    Matching the scale point set
    :param base_path: the path of "data"
    :param img_name: the name of image
    :param point_list: the scale poinit set
    :return: scale_x,scale_y  --->>>   the horizontal scales,the vertical scales-----[[[x1,y1],[x2,y2],scale_data],....]
    '''
    type_1_list = []
    type_2_list = []
    type_1_start_list = point_list[1] + point_list[4] + point_list[5]
    type_1_end_list = point_list[0] + point_list[4] + point_list[5]
    type_2_start_list = point_list[2] + point_list[4] + point_list[5]
    type_2_end_list = point_list[3] + point_list[4] + point_list[5]
    for i in range(len(type_1_start_list)):
        line = []
        type_1_start = copy.copy(type_1_start_list[i])
        type_1_end = find_min(type_1_start, type_1_end_list, flag=2)
        if type_1_end != []:
            line.append(type_1_start)
            line.append(type_1_end)
            type_1_list.append(line)
    for i in range(len(type_2_start_list)):
        line = []
        type_2_start = copy.copy(type_2_start_list[i])
        type_2_end = find_min(type_2_start, type_2_end_list, flag=1)
        if type_2_end != []:
            line.append(type_2_start)
            line.append(type_2_end)
            type_2_list.append(line)
    scale_x = type_2_list
    scale_y = type_1_list
    return scale_x, scale_y

# def tag_ocr(img, scale_x, scale_y, dict_box):
#     ocr = PaddleOCR(use_angle_cls=True, use_gpu=False)
#     scale_x_len = []
#     for i in range(len(scale_x)):
#         scale_x[i][0][1] = scale_x[i][1][1]
#         scale_x_len.append(scale_x[i][1][0] - scale_x[i][0][1])
#         y1 = scale_x[i][0][1] - 30
#         if y1 < 0:
#             y1 = 0
#         y2 = scale_x[i][1][1] + 30
#         if y2 > img.shape[0]:
#             y2 = img.shape[0]
#         x1 = scale_x[i][0][0] - 10
#         if x1 < 0:
#             x1 = 0
#         x2 = scale_x[i][1][0] + 10
#         if x2 > img.shape[1]:
#             x2 = img.shape[1]
#         scale_midx = (x2 - x1) / 2  # *image_x
#         scale_midy = (y2 - y1) / 2  # *image_y
#         img1 = img[int(y1):int(y2), int(x1):int(x2), :].copy()
#         binary = img1
#         # 防止图片太小
#         if binary.shape[1] < 200:
#             binary = np.lib.pad(binary, (
#             (0, 200 // binary.shape[1] * binary.shape[0] - binary.shape[0]), (0, 200 - binary.shape[1]), (0, 0)),
#                                 'constant', constant_values=(255))
#         result = ocr.ocr(binary, cls=True)
#         result1 = ''
#         if result != []:
#             ocr_midx = (result[0][0][0][0] + result[0][0][1][0]) / 2
#             ocr_midy = (result[0][0][0][1] + result[0][0][2][1]) / 2
#             scale_minxy = (ocr_midx - scale_midx) * (ocr_midx - scale_midx) + (ocr_midy - scale_midy) * (
#                         ocr_midy - scale_midy)
#             result1 = result[0][1][0]
#             for j in range(len(result)):
#                 ocr_midx = (result[j][0][0][0] + result[j][0][1][0]) / 2
#                 ocr_midy = (result[j][0][0][1] + result[j][0][2][1]) / 2
#                 if scale_minxy > ((ocr_midx - scale_midx) * (ocr_midx - scale_midx) + (ocr_midy - scale_midy) * (
#                         ocr_midy - scale_midy)):
#                     scale_minxy = ((ocr_midx - scale_midx) * (ocr_midx - scale_midx) + (ocr_midy - scale_midy) * (
#                                 ocr_midy - scale_midy))
#                     result1 = result[j][1][0]
#             if not result1.isdigit():
#                 result1 = ''
#         # 防止识别反字符串
#         if result1 != '' and result1[0] == '0':
#             result1 = result1[::-1]  # 反转字符串
#         scale_x[i].append(result1)
#     scale_y_len = []
#     for i in range(len(scale_y)):
#         scale_y[i][0][0] = scale_y[i][1][0]
#         scale_y_len.append(scale_y[i][1][1] - scale_y[i][0][1])
#         y1 = scale_y[i][0][1] - 10
#         if y1 < 0:
#             y1 = 0
#         y2 = scale_y[i][1][1] + 10
#         if y2 > img.shape[0]:
#             y2 = img.shape[0]
#         x1 = scale_y[i][0][0] - 30
#         if x1 < 0:
#             x1 = 0
#         x2 = scale_y[i][1][0] + 30
#         if x2 > img.shape[1]:
#             x2 = img.shape[1]
#         # 因为顺时针旋转90，所以中心点x,y互换
#         scale_midx = (y2 - y1) / 2  # *image_y
#         scale_midy = (x2 - x1) / 2  # *image_x
#         img1 = img[int(y1):int(y2), int(x1):int(x2), :].copy()
#         binary = img1  # cv2.adaptiveThreshold(gray, 255, cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY, 5, 2)
#         binary = np.rot90(binary, k=-1)  # 顺时针旋转90度
#         if binary.shape[1] < 200:
#             binary = np.lib.pad(binary, (
#             (0, 200 // binary.shape[1] * binary.shape[0] - binary.shape[0]), (0, 200 - binary.shape[1]), (0, 0)),
#                                 'constant', constant_values=(255))
#         result = ocr.ocr(binary, cls=True)
#         result1 = ''
#         if result != []:
#             ocr_midx = (result[0][0][0][0] + result[0][0][1][0]) / 2
#             ocr_midy = (result[0][0][0][1] + result[0][0][2][1]) / 2
#             scale_minxy = (ocr_midx - scale_midx) * (ocr_midx - scale_midx) + (ocr_midy - scale_midy) * (
#                     ocr_midy - scale_midy)
#             result1 = result[0][1][0]
#             for j in range(len(result)):
#                 ocr_midx = (result[j][0][0][0] + result[j][0][1][0]) / 2
#                 ocr_midy = (result[j][0][0][1] + result[j][0][2][1]) / 2
#                 if scale_minxy > ((ocr_midx - scale_midx) * (ocr_midx - scale_midx) + (ocr_midy - scale_midy) * (
#                         ocr_midy - scale_midy)):
#                     scale_minxy = ((ocr_midx - scale_midx) * (ocr_midx - scale_midx) + (ocr_midy - scale_midy) * (
#                                 ocr_midy - scale_midy))
#                     result1 = result[j][1][0]
#             if not result1.isdigit():
#                 result1 = ''
#         if result1 != '' and result1[0] == '0':
#             result1 = result1[::-1]
#         scale_y[i].append(result1)
#     tag = scale_y + scale_x
#     for i in range(len(tag)):
#         # print("**************正在处理第" + str(i) + '个标尺**************')
#         article = {'loaction': [tag[i][0], tag[i][1]], 'id': str(i), 'semantic': tag[i][2]}
#         dict_box['标尺' + str(i)] = article
#     return dict_box

# def device_ocr(image, rectanges, dict_box):
#     ocr = PaddleOCR(use_angle_cls=True, use_gpu=False)
#     for i in range(len(rectanges)):
#         img1 = image[int(rectanges[i][0][1]):int(rectanges[i][1][1]),
#                int(rectanges[i][0][0]):int(rectanges[i][1][0])].copy()
#         result = ocr.ocr(img1, cls=True)
#         word = ''
#         for line in result:
#             word = word + line[1][0]
#         if word == '':
#             word = '普通设备'
#             article = {'loaction': rectanges[i], 'id': str(i), 'semantic': word}
#         else:
#             article = {'loaction': rectanges[i], 'id': str(i), 'semantic': word}
#         print(article)
#         dict_box['box' + str(i)] = article
#     return dict_box

def line_hole_ocr(line_hole_rectanges, dict_box):
    for i in range(len(line_hole_rectanges)):
        article = {'loaction': line_hole_rectanges[i], 'id': str(i), 'semantic': '馈线洞'}
        dict_box['馈线洞' + str(i)] = article
    return dict_box

def gate_ocr(gates, dict_box):
    for i in range(len(gates)):
        article = {'loaction': [gates[i]], 'id': str(i), 'semantic': '门'}
        dict_box['门' + str(i)] = article
    return dict_box

def all_table_ocr(dict_box, alltable=[]):
    if alltable != []:
        article = {'semantic': alltable}
        dict_box['设备表'] = article
    return dict_box

def black_sides_ocr(black_rectanges, dict_box):
    for i in range(len(black_rectanges)):
        article = {'loaction': black_rectanges[i], 'id': str(i), 'semantic': '柱子'}
        dict_box['柱子' + str(i)] = article
    return dict_box

def remaining_area_ocr(dict_box, all_area, all_rectange, remain_area):
    dict_box["像素面积"] = {'全部面积': all_area, '设备面积': all_rectange, '剩余面积': remain_area}
    return dict_box

#
# def remaining_area(rectanges, black_rectanges, wall):
#     '''
#     计算剩余面积
#     '''
#     all_area=0.0
#     all_area=all_area+(wall[1][1][0]-wall[1][0][0])*(wall[1][3][1]-wall[1][0][1])
#     print('全部面积为：', all_area)
#
#     all_rectange=0.0
#     for rectange in rectanges:
#         all_rectange = all_rectange + (rectange[1][0] - rectange[0][0]) * (rectange[3][1] - rectange[0][1])
#     for rectange in black_rectanges:
#         all_rectange = all_rectange + (rectange[1][0] - rectange[0][0]) * (rectange[3][1] - rectange[0][1])
#     print('设备面积为：', all_rectange)
#     remain_area = all_area - all_rectange
#     return all_area,all_rectange,remain_area
#
#