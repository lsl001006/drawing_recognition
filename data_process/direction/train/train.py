
import sys,os
os.environ["CUDA_VISIBLE_DEVICES"] = "0,1,2,3"
import torch.nn as nn
import time
from gen_data import *
from Transformers import Mouse_resized
import torch.utils.data.dataloader
from utils import AverageMeter
from pre_hourglass import test_erro
from posenet import PoseNet
from loss import Constraint_xy_error
from tqdm import tqdm
torch.backends.cudnn.enabled = True
torch.backends.cudnn.benchmark = True

if __name__ == '__main__':
    ##-------------------------------- 路径 ----------------------------------------
    parent_path = '/data1/shanglin/data_process/direction/data/data1129'
    label_path = parent_path+'/train/label'
    img_path = parent_path+'/train/img'
    label_path_val = parent_path+'/val/label'
    img_path_val = parent_path+'/val/img'

    model_save_path = '/data1/shanglin/data_process/direction/exp/aug1201'
    model_path = './models/'
    last_model = model_save_path+"/model_last.pth"
    ##-------------------------------- 参数 ------------------------------------------
    epoch = 0
    TotalEpochs = 200
    nstack = 6
    inp_dim = 256
    oup_dim = 5 # 输出类别数，FIXME
    bn = False
    increase = 0

    model = PoseNet(nstack=nstack, inp_dim=inp_dim, oup_dim=oup_dim, bn=False, increase=0) #.cuda()
    optimizer = torch.optim.Adam(model.parameters(), lr=1e-4)
    if os.path.exists(last_model):
        model = torch.load(last_model)
        with open(os.path.join(model_save_path,"last_epoch.txt"),'r') as f:
            # 读入上次训练到的epoch
            epoch_and_loss = f.readlines()
            last_epoch = int(epoch_and_loss[0].split("\n")[0])
            last_loss = float(epoch_and_loss[1])
        print(f">>> training process resumed from epoch {last_epoch} loss {last_loss}<<<")
    else:
        if torch.cuda.device_count() > 1:
            model = nn.DataParallel(model,device_ids=[0,1,2,3])
    model = model.cuda()

    criterion = nn.MSELoss().cuda()
    train_losses = AverageMeter()
    val_losses = AverageMeter()
    min_losses = 180
    last_loss = 1.0
    
    while epoch < TotalEpochs:
        t1 = time.time()
        print(f"EPOCHS: {epoch}/{TotalEpochs}  ==>> {round(epoch/TotalEpochs*100, 2)} %")

        """--------------------------Train---------------------------"""
        data_train = LSP_DATA(img_path, label_path,  4, Mouse_resized())
        data_val = LSP_DATA(img_path_val, label_path_val,  4, Mouse_resized())

        train_loader = torch.utils.data.dataloader.DataLoader(data_train, batch_size=32, drop_last=True, shuffle=True)
        val_loader = torch.utils.data.dataloader.DataLoader(data_val, batch_size=1, drop_last=True, shuffle=True)

        for j, data in tqdm(enumerate(train_loader)):
            inputs, heatmap = data
            inputs = inputs.cuda()
            heatmap = heatmap.cuda()
            input_var = torch.autograd.Variable(inputs)
            heatmap_var = torch.autograd.Variable(heatmap)
            result= model(input_var)
            loss = 0
            for i in range(nstack):
                loss1 = criterion(result[:, i, :, :, :], heatmap_var)
                loss = loss + loss1
            loss = loss / nstack
            train_losses.update(loss.item(), inputs.size(0))
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            # print(f'[loss]: {loss}')
        t2 = time.time()
        print(f'> training time: {round(t2-t1,2)}s   Train Loss: {train_losses.avg}')
        torch.save(model, model_save_path + "/model_last.pth")
        with open(os.path.join(model_save_path,"last_epoch.txt"),'w') as f:
            f.write(str(epoch)+"\n"+str(train_losses.avg))
        if last_loss > train_losses.avg:
            min_loss = train_losses.avg
            torch.save(model, model_save_path + "/model_best.pth")
            with open(os.path.join(model_save_path,"best_epoch.txt"),'w') as f:
                f.write(str(epoch)+"\n"+str(train_losses.avg))

        
        if ((epoch + 1) % 5 == 0):
            torch.save(model, model_save_path + '/hourglass_1201_' + str(epoch + 1) + '.pth')
            
        epoch+=1

        #val data
        # model.eval()
        # t3 = time.time()
        # for j, data in enumerate(val_loader):
        #     inputs, heatmap= data
        #     inputs = inputs.cuda()
        #     heatmap = heatmap.cuda()
        #     input_var = torch.autograd.Variable(inputs)
        #     heatmap_var = torch.autograd.Variable(heatmap)

        #     result= model(input_var)
        #     loss = 0
        #     for i in range(nstack):
        #         loss1 = criterion(result[:, i, :, :, :], heatmap_var)
        #         loss = loss + loss1
        #     loss = loss / nstack
        #     val_losses.update(loss.item(), inputs.size(0))
        #     optimizer.zero_grad()
        #     loss.backward()
        #     optimizer.step()

        # t4 = time.time()
        # print('val time: ', t4 - t3)
        # print('Val Loss: ', val_losses.avg)
        # test_erro_img = test_erro(model_save_path)

        # if test_erro_img < min_losses:
        #     torch.save(model, best_model_path)
        #     min_losses = test_erro_img
        # model.train()
        # epoch += 1

