import matplotlib
# matplotlib.use('AGG')#或者PDF, SVG或PS /
import matplotlib.pyplot as plt
import numpy as np
import sys
import os


def get_loss_info(log_file):
    with open(log_file) as loss_data:
        all_lines = loss_data.readlines()

        train_total_loss = []          # 4
        train_hm_loss = []             # 7
        train_wh_loss = []             # 10
        train_off_loss = []            # 13
        train_spend_time = []          # 16

        val_total_loss = []            # 19
        val_hm_loss = []               # 22
        val_wh_loss = []               # 25
        val_off_loss = []              # 28
        val_spend_time = []            # 31    
        num_lines = len(all_lines)
        index_val = np.array([])
        for lineCnt,line in enumerate(range(num_lines)):
            elements = all_lines[line].split(' ')
    
            train_total_loss.append(float(elements[4]))
            train_hm_loss.append(float(elements[7]))
            train_wh_loss.append(float(elements[10]))
            train_off_loss.append(float(elements[13]))
            train_spend_time.append(float(elements[16]))

            if len(elements) > 22:
                index_val = np.append(index_val, lineCnt)
                val_total_loss.append(float(elements[19]))
                val_hm_loss.append(float(elements[22]))
                val_wh_loss.append(float(elements[25]))
                val_off_loss.append(float(elements[28]))
                val_spend_time.append(float(elements[31]))

        train_info = {'total_loss':train_total_loss, 'hm_loss':train_hm_loss, 'wh_loss':train_wh_loss, 'off_loss':train_off_loss, 'spend_time':train_spend_time}
        val_info = {'total_loss':val_total_loss, 'hm_loss':val_hm_loss, 'wh_loss':val_wh_loss, 'off_loss':val_off_loss, 'spend_time':val_spend_time, 'index':index_val}

    return train_info, val_info
 
 
if __name__ == '__main__':
    plt.ion()
    # 标准图形绘制
    # sns.set()
    log_path = sys.argv[1]
    # log_path = "exp/ctdet/4840继续训练/logs_2021-06-04-03-05/log.txt"
    log_abs_path = os.path.abspath(log_path)
    train_info, val_info = get_loss_info(log_abs_path)              # 读取训练时生成的日志文件

    ######################## 绘制 总loss 图
    fig = plt.figure(figsize=(15, 8))
    ax = fig.add_subplot(221)
    ax.plot(range(len(train_info['total_loss'])), train_info['total_loss'], 'b-', label='train_total_loss', linewidth=1)
    ax.plot(val_info['index'], val_info['total_loss'], 'r-', label='val_total_loss', linewidth=1)
    ax.set_ylabel('loss_value')
    ax.set_title('train total loss and val total loss')
    ax.legend(loc='best')                                      # 将图例摆放在不遮挡图线的位置即可
    ax.grid()                                                  # 添加网格    

    ######################## 绘制 hm_loss 图
    ax = fig.add_subplot(222)
    ax.plot(range(len(train_info['hm_loss'])), train_info['hm_loss'], 'b-', label='train_hm_loss', linewidth=1)
    ax.plot(val_info['index'], val_info['hm_loss'], 'r-', label='val_hm_loss', linewidth=1)
    ax.set_ylabel('loss_value')
    ax.set_title('train hm loss and val hm loss')
    ax.legend(loc='best')                                      # 将图例摆放在不遮挡图线的位置即可
    ax.grid()                                                  # 添加网格

    ######################## 绘制 wh_loss 图
    ax = fig.add_subplot(223)
    ax.plot(range(len(train_info['wh_loss'])), train_info['wh_loss'], 'b-', label='train_wh_loss', linewidth=1)
    ax.plot(val_info['index'], val_info['wh_loss'], 'r-', label='val_wh_loss', linewidth=1)
    ax.set_ylabel('loss_value')
    ax.set_title('train wh loss and val wh loss')
    ax.legend(loc='best')                                      # 将图例摆放在不遮挡图线的位置即可
    ax.grid()                                                  # 添加网格

    ######################## 绘制 off_loss 图
    ax = fig.add_subplot(224)
    ax.plot(range(len(train_info['off_loss'])), train_info['off_loss'], 'b-', label='train_off_loss', linewidth=1)
    ax.plot(val_info['index'], val_info['off_loss'], 'r-', label='val_off_loss', linewidth=1)
    ax.set_ylabel('loss_value')
    ax.set_title('train off loss and val off loss')
    ax.legend(loc='best')                                      # 将图例摆放在不遮挡图线的位置即可
    ax.grid()                                                  # 添加网格

    log_dir_path = os.path.dirname(log_abs_path)
    fig_path = os.path.join(log_dir_path, 'loss.png')
    plt.savefig(fig_path)                                    # 保存文件到指定文件夹
    plt.ioff()
    plt.show()
 
