from logging import raiseExceptions
import os
import json
import PIL.Image
import PIL.ImageDraw
import numpy as np
from utils import create_dir, list_jsons_recursively
from image_utils import read_image_shape_as_dict
from tqdm import tqdm


class labelme2coco(object):
    def __init__(self, label_file_path, labelme_folder, save_json_path='./new.json'):
        """
        Args:
            labelme_folder: folder that contains labelme annotations and image files
            save_json_path: path for coco json to be saved
        """
        self.save_json_path = save_json_path
        self.images = []
        self.annotations = []
        # label从label文件中获取，顺序编号固定
        self.labels_id = []
        self.labels = []
        self.categories = []
        f_label = open(label_file_path)
        for line in f_label:
            label_id, label = line.strip().split()
            self.labels.append(label)
            self.labels_id.append(int(label_id))
            category = {}
            category['supercategory'] = label
            category['id'] = int(label_id)
            category['name'] = label
            self.categories.append(category)
        f_label.close()
        
        self.annID = 1
        self.height = 0
        self.width = 0

        # create save dir
        save_json_dir = os.path.dirname(save_json_path)
        create_dir(save_json_dir)

        # get json list
        _, labelme_json = list_jsons_recursively(labelme_folder)
        self.labelme_json = labelme_json

        self.save_json()

    def data_transfer(self):
        # num决定image_id号
        for num, json_path in tqdm(enumerate(self.labelme_json)):
            with open(json_path, 'rb') as fp:
                # load json
                data = json.load(fp)
#                (prefix, res) = os.path.split(json_path)
#                (file_name, extension) = os.path.splitext(res)
                self.images.append(self.image(data, num, json_path))
                for shape in data['shapes']:
                    label = shape['label']
                    if label == 'mask':
                        continue
                    if len(shape['points'])!=2:
                        continue
                    # 异常检测！
                    if label not in self.labels:
                        continue
                    points = shape['points']
                    self.annotations.append(self.annotation(points, label, num, data, json_path))
                    self.annID += 1 # 标记的总计数器，跨图片，一直累加

    def image(self, data, num, json_path):
        image = {}
        # get image path
        _, img_extension = os.path.splitext(data["imagePath"])
        image_path = json_path.replace(".json", img_extension)
        img_shape = read_image_shape_as_dict(image_path)
        height, width = img_shape['height'], img_shape['width']

        image['height'] = height
        image['width'] = width
        image['id'] = int(num + 1)
        image['file_name'] = image_path

        self.height = height
        self.width = width

        return image

    def annotation(self, points, label, num, data, json_path):

        _, img_extension = os.path.splitext(data["imagePath"])
        image_path = json_path.replace(".json", img_extension)
        annotation = {}
        annotation['iscrowd'] = 0
        annotation['image_id'] = int(num + 1)
        annotation['file_name'] = image_path.split('/')[-1].split('.')[0]

        x1, x2 = min(points[0][0], points[1][0]), max(points[0][0], points[1][0])
        y1, y2 = min(points[0][1], points[1][1]), max(points[0][1], points[1][1])
        w, h = x2-x1, y2-y1

        annotation['bbox'] = [x1, y1, w, h]
        
        # 设置掩码为bbox框的四个点
        annotation['segmentation'] = [[x1,y1,x2,y1,x2,y2,x1,y2]]

        annotation['category_id'] = self.labels_id[self.labels.index(label)]
        annotation['id'] = int(self.annID) # 这个id是标注的id号，一直累加的
        # add area info
        annotation['area'] = self.height * self.width  # the area is not used for detection
        return annotation

    def getbbox(self,points):
        # img = np.zeros([self.height,self.width],np.uint8)
        # cv2.polylines(img, [np.asarray(points)], True, 1, lineType=cv2.LINE_AA)
        # cv2.fillPoly(img, [np.asarray(points)], 1)
        polygons = points
        mask = self.polygons_to_mask([self.height, self.width], polygons)
        return self.mask2box(mask)

    def mask2box(self, mask):
        # np.where(mask==1)
        index = np.argwhere(mask == 1)
        rows = index[:, 0]
        clos = index[:, 1]

        left_top_r = np.min(rows)  # y
        left_top_c = np.min(clos)  # x

        right_bottom_r = np.max(rows)
        right_bottom_c = np.max(clos)

        return [left_top_c, left_top_r, right_bottom_c-left_top_c, right_bottom_r-left_top_r]  # [x1,y1,w,h] for coco box format

    def polygons_to_mask(self, img_shape, polygons):
        mask = np.zeros(img_shape, dtype=np.uint8)
        mask = PIL.Image.fromarray(mask)
        xy = list(map(tuple, polygons))
        PIL.ImageDraw.Draw(mask).polygon(xy=xy, outline=1, fill=1)
        mask = np.array(mask, dtype=bool)
        return mask

    def data2coco(self):
        data_coco = {}
        data_coco['images'] = self.images
        data_coco['categories'] = self.categories
        data_coco['annotations'] = self.annotations
        return data_coco

    def save_json(self):
        self.data_transfer()
        self.data_coco = self.data2coco()

        json.dump(self.data_coco, open(self.save_json_path, 'w', encoding='utf-8'),ensure_ascii=False, indent=4, separators=(',', ': '), cls=MyEncoder)


# type check when save json files
class MyEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(MyEncoder, self).default(obj)

