from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import pycocotools.coco as coco
from pycocotools.cocoeval import COCOeval
import numpy as np
import json
import os
import torch
from pathlib import Path

import torch.utils.data as data


class DRAW3(data.Dataset):
  num_classes = 69
  default_resolution = [512, 512]
  mean = np.array([0.025883,0.025978, 0.025996],
                   dtype=np.float32).reshape(1, 1, 3)
  std  = np.array([0.136837, 0.137065,0.137109],
                   dtype=np.float32).reshape(1, 1, 3)
  
  gray_mean = np.array([0.025883], dtype=np.float32).reshape(1, 1, 1)
  gray_std = np.array([0.13687], dtype=np.float32).reshape(1, 1, 1)

  def __init__(self, opt, split):
    super(DRAW3, self).__init__()
    self.data_dir = os.path.join('/data1/shanglin/data_process', 'cocodata1206')
    #self.img_dir = os.path.join(self.data_dir, 'images')
    self.img_dir = self.data_dir
    if split == 'val':
      self.annot_path = os.path.join(
          self.data_dir, 'annotations', 
          'val.json')
    else:
      if opt.task == 'exdet':
        self.annot_path = os.path.join(
          self.data_dir, 'annotations', 
          'train.json')
      else:
        self.annot_path = os.path.join(
          self.data_dir, 'annotations', 
          'train.json')
    self.max_objs = 128
    self.class_name = [
      'a1-1', 'a1-2', 'a1-3', 'a1-4', 'a3', 'a4', 'an', 'b1', 'c1', 'd1', 'g1', 'h1', 'l1', 'n1', 'p1-1', 
      'p1-2', 's1', 't1', 't10', 't3-l', 't3-m', 't3-r', 't7', 't9', 'tn', 'v1','va1', 'va3', 'va4', 'van',
      'vb1', 'vp1-1', 'vp1-2', 'vp1-3', 'vp1-4', 'vp1-5', 'vp1-6', 'vp2-1', 'vp2-2', 'vp2-3', 'vpn', 'vs1', 
      'vt1', 'vt10-1', 'vt10-2', 'vt10-n', 'vt3', 'vt4', 'vt7-1', 'vt7-2', 'vt9', 'vtn', 'z1', 'x1', 'k1', 
      'x2', 'btl', 'tl', 'sm', 'wz1', 'wz1-l', 'wz2', 'wz3', 'wz3-l', 'wz4','j1','xtl','wztl','wlb']
    self._valid_ids = list(range(1,self.num_classes+1))
    self.cat_ids = {v: i for i, v in enumerate(self._valid_ids)}
    self.voc_color = [(v // 32 * 64 + 64, (v // 8) % 4 * 64, v % 8 * 32) \
                      for v in range(1, self.num_classes + 1)]
    self._data_rng = np.random.RandomState(123)
    self._eig_val = np.array([0.2141788, 0.01817699, 0.00341571],
                             dtype=np.float32)
    self._eig_vec = np.array([
        [-0.58752847, -0.69563484, 0.41340352],
        [-0.5832747, 0.00994535, -0.81221408],
        [-0.56089297, 0.71832671, 0.41158938]
    ], dtype=np.float32)
    # self.mean = np.array([0.485, 0.456, 0.406], np.float32).reshape(1, 1, 3)
    # self.std = np.array([0.229, 0.224, 0.225], np.float32).reshape(1, 1, 3)

    self.split = split
    self.opt = opt

    print('==> initializing tower recognition {} data.'.format(split))
    self.coco = coco.COCO(self.annot_path)
    self.images = self.coco.getImgIds()
    self.num_samples = len(self.images)

    print('Loaded {} {} samples'.format(split, self.num_samples))
    # print(torch.cuda.current_device())

  def _to_float(self, x):
    return float("{:.2f}".format(x))

  def convert_eval_format(self, all_bboxes):
    # import pdb; pdb.set_trace()
    detections = []
    img_dir = list(Path(os.path.join(self.data_dir, 'valSet')).rglob('*.json'))
    img_dir.sort()
    for image_id in all_bboxes:
      for cls_ind in all_bboxes[image_id]:
        category_id = self._valid_ids[cls_ind - 1]
        for bbox in all_bboxes[image_id][cls_ind]:
          bbox[2] -= bbox[0]
          bbox[3] -= bbox[1]
          score = bbox[4]
          bbox_out  = list(map(self._to_float, bbox[0:4]))
          tmpfn = str(img_dir[int(image_id)-1])
          detection = {
              "image_id": int(image_id),
              "file_name": tmpfn.split('/')[-1].split('.')[0],
              "category_id": int(category_id),
              "bbox": bbox_out,
              "score": float("{:.2f}".format(score))
          }
          if len(bbox) > 5:
              extreme_points = list(map(self._to_float, bbox[5:13]))
              detection["extreme_points"] = extreme_points
          detections.append(detection)
    return detections

  def __len__(self):
    return self.num_samples

  def save_results(self, results, save_dir):
    json.dump(self.convert_eval_format(results), 
                open('{}/results.json'.format(save_dir), 'w', encoding='utf-8'),ensure_ascii=False)
  
  def run_eval(self, results, save_dir):
    # result_json = os.path.join(save_dir, "results.json")
    # detections  = self.convert_eval_format(results)
    # json.dump(detections, open(result_json, "w"))
    self.save_results(results, save_dir)
    coco_dets = self.coco.loadRes('{}/results.json'.format(save_dir))
    coco_eval = COCOeval(self.coco, coco_dets, "bbox")
    coco_eval.evaluate()
    coco_eval.accumulate()
    coco_eval.summarize()
