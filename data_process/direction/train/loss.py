import torch
import numpy as np
from tools_pre import find_min_loss

class HeatmapLoss(torch.nn.Module):
    """
    loss for detection heatmap
    """
    def __init__(self):
        super(HeatmapLoss, self).__init__()

    def forward(self, pred, gt):
        l = ((pred - gt)**2)
        l = l.mean(dim=3).mean(dim=2).mean(dim=1)
        return l ## l of dim bsize

class Constraint_xy_error(torch.nn.Module):
    def __init__(self):
        super(Constraint_xy_error, self).__init__()

    def get_key_points(self,heatmap, height, width):
        # Get final heatmap
        heatmap = np.asarray(heatmap.cpu().data)
        key_points = [[] for i in range(23)]################
        # Get k key points from heatmap6
        for i in range(len(heatmap)):
            x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
            a = [x, y]
            b = heatmap[i]
            key = []
            # Get the coordinate of key point in the heatmap (46, 46)
            while (a != [] and heatmap[i][a[0]][a[1]] > 0.5):
                key.append(a)
                for j in range(-2, 2):
                    for z in range(-2, 2):
                        if a[0] + j < 64 and a[1] + z < 64:
                            heatmap[i][a[0] + j][a[1] + z] = 0
                x, y = np.unravel_index(np.argmax(heatmap[i]), heatmap[i].shape)
                a = [x, y]
                # Calculate the scale to fit original image
            scale_x = width / heatmap[i].shape[1]
            scale_y = height / heatmap[i].shape[0]
            for z in key:
                y = int(z[0] * scale_x)
                x = int(z[1] * scale_y)
                key_points[i].append([x, y])
        return key_points

    def xy_constraint_error(self,point_list):
        point_equipment = point_list[6:15]
        side1 = []
        side2 = []
        #################横线误差##############
        # 横线搜索范围
        side_equipment_Horizontal = point_equipment[1] + point_equipment[3] + point_equipment[5] + point_equipment[6] + \
                                    point_equipment[7] + point_equipment[8]
        side1 = side1 + find_min_loss(point_equipment[0], side_equipment_Horizontal, 1)
        side1 = side1 + find_min_loss(point_equipment[2], side_equipment_Horizontal, 1)
        side1 = side1 + find_min_loss(point_equipment[4], side_equipment_Horizontal, 1)
        side1 = side1 + find_min_loss(point_equipment[6], side_equipment_Horizontal, 1)
        side1 = side1 + find_min_loss(point_equipment[7], side_equipment_Horizontal, 1)
        side1 = side1 + find_min_loss(point_equipment[8], side_equipment_Horizontal, 1)
        y_err = 0
        if len(side1) == 0:
            y_err = 0
        else:
            for i in range(len(side1)):
                y_err = y_err + abs(side1[i][0][1] - side1[i][1][1])
            y_err = y_err / len(side1)
        #################竖线误差##############
        # 竖线搜索范围
        side_equipment_vertical = point_equipment[2] + point_equipment[3] + point_equipment[4] + point_equipment[5] + \
                                  point_equipment[7] + point_equipment[8]
        side2 = side2 + find_min_loss(point_equipment[0], side_equipment_vertical, 2)
        side2 = side2 + find_min_loss(point_equipment[1], side_equipment_vertical, 2)
        side2 = side2 + find_min_loss(point_equipment[4], side_equipment_vertical, 2)
        side2 = side2 + find_min_loss(point_equipment[5], side_equipment_vertical, 2)
        side2 = side2 + find_min_loss(point_equipment[6], side_equipment_vertical, 2)
        side2 = side2 + find_min_loss(point_equipment[8], side_equipment_vertical, 2)
        x_err = 0
        if len(side2) == 0:
            x_err = 0
        else:
            for i in range(len(side2)):
                x_err = x_err + abs(side2[i][0][0] - side2[i][1][0])
            x_err = x_err / len(side2)
        return x_err, y_err

    def forward(self,heatmap):
        x_loss = []
        y_loss = []
        for i in range(heatmap.shape[0]):
            key_points=self.get_key_points(heatmap[i], 256, 256)
            x_err,y_err=self.xy_constraint_error(key_points)
            x_loss.append(x_err)
            y_loss.append(y_err)
        x_loss = torch.mean(torch.Tensor(x_loss))
        y_loss = torch.mean(torch.Tensor(y_loss))

        return x_loss,y_loss ## l of dim bsize
